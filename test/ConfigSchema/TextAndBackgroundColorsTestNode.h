#pragma once

#include <ApplicationConfig.h>
#include <ApplicationConfigSchema.h>
#include <ObjectSchemaKey.h>
#include <SchemaHelpers.h>
#include <SchemaKey.h>
#include <TextAndBackgroundColors.h>
#include <array>
#include <functional>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <memory>

using namespace application::config::schema;

struct TextAndBackgroundColorsTestNode {
  MOCK_METHOD(bool, exists, (), (const, noexcept));
  MOCK_METHOD(std::string, GetStringValue, (), (const, noexcept));
  MOCK_METHOD(unsigned int, GetUnsignedValue, (), (const, noexcept));
  MOCK_METHOD(int, GetIntValue, (), (const, noexcept));
  MOCK_METHOD(TextAndBackgroundColorsTestNode&, GetForeground, (),
              (const, noexcept));
  MOCK_METHOD(TextAndBackgroundColorsTestNode&, GetBackground, (),
              (const, noexcept));
  template <typename T>
  auto get_value() const noexcept {
    constexpr bool IS_STRING = std::is_same_v<T, std::string>;
    if constexpr (IS_STRING)
      return GetStringValue();
    else
      static_assert(!IS_STRING, "Unexpected return value type");
  }
  TextAndBackgroundColorsTestNode() = default;
  TextAndBackgroundColorsTestNode(const TextAndBackgroundColorsTestNode&) {}
};

template <>
struct Helpers<TextAndBackgroundColorsTestNode> {
  static bool
  node_exists(const TextAndBackgroundColorsTestNode& node) noexcept {
    return node.exists();
  }

  template <typename NodeValueType>
  static NodeValueType
  get_node_value(const TextAndBackgroundColorsTestNode& node) noexcept {
    return node.get_value<NodeValueType>();
  }

  static const TextAndBackgroundColorsTestNode&
  node_under_key(const TextAndBackgroundColorsTestNode& parent,
                 ConfigSchemaKey config_key) {
    if (config_key == ConfigSchemaKey::FOREGROUND)
      return parent.GetForeground();

    if (config_key == ConfigSchemaKey::BACKGROUND)
      return parent.GetBackground();

    return parent;
  }
};

