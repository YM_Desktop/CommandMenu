#include "ApplicationConfig.h"
#include "CommandMenuConfig.h"
#include "TextAndBackgroundColors.h"

#include <ApplicationConfigSchema.h>
#include <ObjectSchemaKey.h>
#include <SchemaHelpers.h>
#include <SchemaKey.h>
#include <array>
#include <filesystem>
#include <functional>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <memory>
#include <stdexcept>

using namespace application::config::schema;

struct MenuConfigTestNode {
  MOCK_METHOD(bool, exists, (), (const, noexcept));
  MOCK_METHOD(application::cmd_menu::CustomSearchPathsType,
              get_custom_search_paths, (), (const, noexcept));
  MOCK_METHOD(bool, get_use_system_path, (), (const, noexcept));
  MOCK_METHOD(MenuConfigTestNode&, custom_search_paths, (), (const, noexcept));
  MOCK_METHOD(MenuConfigTestNode&, use_system_path, (), (const, noexcept));
  template <typename T>
  auto get_value() const noexcept {
    constexpr bool IS_CUSTOM_SEARCH_PATHS =
        std::is_same_v<T, application::cmd_menu::CustomSearchPathsType>;
    if constexpr (IS_CUSTOM_SEARCH_PATHS)
      return get_custom_search_paths();

    constexpr bool IS_USE_SYSTEM_PATH = std::is_same_v<T, bool>;
    if constexpr (IS_USE_SYSTEM_PATH)
      return get_use_system_path();

    static_assert(IS_CUSTOM_SEARCH_PATHS || IS_USE_SYSTEM_PATH,
                  "Unexpected return value type");
  }
  MenuConfigTestNode() = default;
  MenuConfigTestNode(const MenuConfigTestNode&) {}
};

template <>
struct Helpers<MenuConfigTestNode> {
  static bool node_exists(const MenuConfigTestNode& node) noexcept {
    return node.exists();
  }

  template <typename NodeValueType>
  static NodeValueType get_node_value(const MenuConfigTestNode& node) noexcept {
    return node.get_value<NodeValueType>();
  }

  static const MenuConfigTestNode&
  node_under_key(const MenuConfigTestNode& parent, ConfigSchemaKey config_key) {
    if (config_key == ConfigSchemaKey::CUSTOM_SEARCH_PATHS)
      return parent.custom_search_paths();

    if (config_key == ConfigSchemaKey::USE_SYSTEM_PATH)
      return parent.use_system_path();

    throw std::runtime_error("Unexpected key for node");
  }
};

struct CommandMenuConfigSchemaTest : public testing::Test {
  std::unique_ptr<application::ApplicationConfig> cfg;
  MenuConfigTestNode node;
  MenuConfigTestNode custom_search_paths_node, use_system_path_node;
  application::cmd_menu::CommandMenuConfig* menu_cfg;

  void SetUp() override {
    cfg = std::make_unique<application::ApplicationConfig>();
    menu_cfg = &cfg->menu_cfg;
    ON_CALL(node, custom_search_paths)
        .WillByDefault(testing::ReturnRef(custom_search_paths_node));
    ON_CALL(node, use_system_path)
        .WillByDefault(testing::ReturnRef(use_system_path_node));
  }

  void TearDown() override { cfg.reset(); }
};

TEST_F(CommandMenuConfigSchemaTest,
       Validate_CustomPaths_And_UseSystemPath_Exist) {
  const application::cmd_menu::CustomSearchPathsType
      expected_custom_paths_value{std::filesystem::path{"/tmp/path1"},
                                  std::filesystem::path{"/tmp/path2"}};
  const auto expected_use_system_path_value = true;
  EXPECT_CALL(node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(node, custom_search_paths);
  EXPECT_CALL(node, use_system_path);
  EXPECT_CALL(custom_search_paths_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(custom_search_paths_node, get_custom_search_paths)
      .WillOnce(testing::Return(expected_custom_paths_value));
  EXPECT_CALL(use_system_path_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(use_system_path_node, get_use_system_path)
      .WillOnce(testing::Return(expected_use_system_path_value));

  application::config::schema::MENU::validate(node, *menu_cfg);

  EXPECT_EQ(menu_cfg->custom_search_paths, expected_custom_paths_value);
  EXPECT_EQ(menu_cfg->use_system_path, expected_use_system_path_value);
}

TEST_F(CommandMenuConfigSchemaTest,
       Validate_CustomPaths_DoNotExist_And_UseSystemPath_Exist) {
  const auto expected_custom_paths_value = menu_cfg->custom_search_paths;
  const auto expected_use_system_path_value = true;
  EXPECT_CALL(node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(node, custom_search_paths);
  EXPECT_CALL(node, use_system_path);
  EXPECT_CALL(custom_search_paths_node, exists)
      .WillOnce(testing::Return(false));
  EXPECT_CALL(custom_search_paths_node, get_custom_search_paths).Times(0);
  EXPECT_CALL(use_system_path_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(use_system_path_node, get_use_system_path)
      .WillOnce(testing::Return(expected_use_system_path_value));

  application::config::schema::MENU::validate(node, *menu_cfg);

  EXPECT_EQ(menu_cfg->custom_search_paths, expected_custom_paths_value);
  EXPECT_EQ(menu_cfg->use_system_path, expected_use_system_path_value);
}

TEST_F(CommandMenuConfigSchemaTest,
       Validate_CustomPaths_DoNotExist_And_UseSystemPath_DoNotExist) {
  const auto expected_custom_paths_value = menu_cfg->custom_search_paths;
  const auto expected_use_system_path_value = menu_cfg->use_system_path;
  EXPECT_CALL(node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(node, custom_search_paths);
  EXPECT_CALL(node, use_system_path);
  EXPECT_CALL(custom_search_paths_node, exists)
      .WillOnce(testing::Return(false));
  EXPECT_CALL(custom_search_paths_node, get_custom_search_paths).Times(0);
  EXPECT_CALL(use_system_path_node, exists).WillOnce(testing::Return(false));
  EXPECT_CALL(use_system_path_node, get_use_system_path).Times(0);

  EXPECT_THROW(application::config::schema::MENU::validate(node, *menu_cfg),
               std::logic_error);

  EXPECT_EQ(menu_cfg->custom_search_paths, expected_custom_paths_value);
  EXPECT_EQ(menu_cfg->use_system_path, expected_use_system_path_value);
}
