#include <ApplicationConfig.h>
#include <ApplicationConfigSchema.h>
#include <CommandMenuConfig.h>
#include <ObjectSchemaKey.h>
#include <SchemaHelpers.h>
#include <SchemaKey.h>
#include <TextAndBackgroundColors.h>
#include <array>
#include <filesystem>
#include <functional>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <memory>
#include <stdexcept>

using namespace application::config::schema;

struct UIConfigTestNode {
  MOCK_METHOD(bool, exists, (), (const, noexcept));
  MOCK_METHOD(std::string, get_string, (), (const, noexcept));
  MOCK_METHOD(int, get_int, (), (const, noexcept));
  MOCK_METHOD(unsigned int, get_unsigned, (), (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, input_line, (), (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, suggestion_line, (), (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, suggestion_line_selected, (),
              (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, page_shift_inactive, (), (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, page_shift_active, (), (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, window, (), (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, font, (), (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, foreground, (), (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, background, (), (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, name, (), (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, size, (), (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, input_line_char_width, (), (const, noexcept));
  MOCK_METHOD(UIConfigTestNode&, input_prefix_char_width, (),
              (const, noexcept));

  template <typename T>
  auto get_value() const noexcept {
    constexpr bool IS_STRING = std::is_same_v<T, std::string>;
    if constexpr (IS_STRING)
      return get_string();

    constexpr bool IS_INT = std::is_same_v<T, int>;
    if constexpr (IS_INT)
      return get_int();

    constexpr bool IS_UNSIGNED_INT = std::is_same_v<T, unsigned int>;
    if constexpr (IS_UNSIGNED_INT)
      return get_unsigned();

    static_assert(IS_STRING || IS_INT || IS_UNSIGNED_INT,
                  "Unexpected return value type");
  }
  UIConfigTestNode() = default;
  UIConfigTestNode(const UIConfigTestNode&) {}
};

template <>
struct Helpers<UIConfigTestNode> {
  static bool node_exists(const UIConfigTestNode& node) noexcept {
    return node.exists();
  }

  template <typename NodeValueType>
  static NodeValueType get_node_value(const UIConfigTestNode& node) noexcept {
    return node.get_value<NodeValueType>();
  }

  static const UIConfigTestNode& node_under_key(const UIConfigTestNode& parent,
                                                ConfigSchemaKey config_key) {
    if (config_key == ConfigSchemaKey::INPUT_LINE)
      return parent.input_line();

    if (config_key == ConfigSchemaKey::SUGGESTION_LINE)
      return parent.suggestion_line();

    if (config_key == ConfigSchemaKey::SUGGESTION_LINE_SELECTED)
      return parent.suggestion_line_selected();

    if (config_key == ConfigSchemaKey::PAGE_SHIFT_ACTIVE)
      return parent.page_shift_active();

    if (config_key == ConfigSchemaKey::PAGE_SHIFT_INACTIVE)
      return parent.page_shift_inactive();

    if (config_key == ConfigSchemaKey::WINDOW)
      return parent.window();

    if (config_key == ConfigSchemaKey::FONT)
      return parent.font();

    if (config_key == ConfigSchemaKey::NAME)
      return parent.name();

    if (config_key == ConfigSchemaKey::SIZE)
      return parent.size();

    if (config_key == ConfigSchemaKey::FOREGROUND)
      return parent.foreground();

    if (config_key == ConfigSchemaKey::BACKGROUND)
      return parent.background();

    if (config_key == ConfigSchemaKey::INPUT_MAX_WIDTH)
      return parent.input_line_char_width();

    if (config_key == ConfigSchemaKey::PREFIX_MAX_WIDTH)
      return parent.input_prefix_char_width();

    throw std::runtime_error("Unexpected key for node");
  }
};

struct UIConfigSchemaTest : public testing::Test {
  std::unique_ptr<application::ApplicationConfig> cfg;
  UIConfigTestNode root_node;
  UIConfigTestNode input_line_node, suggestion_line_node,
      suggestion_line_selected_node, page_shift_inactive_node,
      page_shift_active_node, window_node, font_node, name_node, size_node,
      foreground_node, background_node, input_line_char_width_node,
      input_prefix_char_width_node;
  application::ui::Config* ui_cfg;

  void SetUp() override {
    cfg = std::make_unique<application::ApplicationConfig>();
    ui_cfg = &cfg->ui_cfg;

    ON_CALL(root_node, input_line)
        .WillByDefault(testing::ReturnRef(input_line_node));
    ON_CALL(input_line_node, foreground)
        .WillByDefault(testing::ReturnRef(foreground_node));
    ON_CALL(input_line_node, background)
        .WillByDefault(testing::ReturnRef(background_node));

    ON_CALL(root_node, suggestion_line)
        .WillByDefault(testing::ReturnRef(suggestion_line_node));
    ON_CALL(suggestion_line_node, foreground)
        .WillByDefault(testing::ReturnRef(foreground_node));
    ON_CALL(suggestion_line_node, background)
        .WillByDefault(testing::ReturnRef(background_node));

    ON_CALL(root_node, suggestion_line_selected)
        .WillByDefault(testing::ReturnRef(suggestion_line_selected_node));
    ON_CALL(suggestion_line_selected_node, foreground)
        .WillByDefault(testing::ReturnRef(foreground_node));
    ON_CALL(suggestion_line_selected_node, background)
        .WillByDefault(testing::ReturnRef(background_node));

    ON_CALL(root_node, page_shift_active)
        .WillByDefault(testing::ReturnRef(page_shift_active_node));
    ON_CALL(page_shift_active_node, foreground)
        .WillByDefault(testing::ReturnRef(foreground_node));
    ON_CALL(page_shift_active_node, background)
        .WillByDefault(testing::ReturnRef(background_node));

    ON_CALL(root_node, page_shift_inactive)
        .WillByDefault(testing::ReturnRef(page_shift_inactive_node));
    ON_CALL(page_shift_inactive_node, foreground)
        .WillByDefault(testing::ReturnRef(foreground_node));
    ON_CALL(page_shift_inactive_node, background)
        .WillByDefault(testing::ReturnRef(background_node));

    ON_CALL(root_node, window).WillByDefault(testing::ReturnRef(window_node));
    ON_CALL(window_node, foreground)
        .WillByDefault(testing::ReturnRef(foreground_node));
    ON_CALL(window_node, background)
        .WillByDefault(testing::ReturnRef(background_node));

    ON_CALL(root_node, font).WillByDefault(testing::ReturnRef(font_node));
    ON_CALL(font_node, name).WillByDefault(testing::ReturnRef(name_node));
    ON_CALL(font_node, size).WillByDefault(testing::ReturnRef(size_node));

    ON_CALL(root_node, input_line_char_width)
        .WillByDefault(testing::ReturnRef(input_line_char_width_node));
    ON_CALL(root_node, input_prefix_char_width)
        .WillByDefault(testing::ReturnRef(input_prefix_char_width_node));
  }

  void TearDown() override { cfg.reset(); }
};

TEST_F(UIConfigSchemaTest, Validate_all_keys_exist) {
  const std::string expected_foreground_value{"#000000"};
  const std::string expected_background_value{"#aaaaaa"};
  const std::string expected_font_name_value{"font name"};
  const int expected_font_size_value{10};
  const unsigned int expected_input_line_char_width{30u};
  const unsigned int expected_input_prefix_char_width{20u};

  EXPECT_CALL(root_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(root_node, input_line);
  EXPECT_CALL(input_line_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(input_line_node, foreground);
  EXPECT_CALL(input_line_node, background);
  EXPECT_CALL(root_node, suggestion_line);
  EXPECT_CALL(suggestion_line_node, foreground);
  EXPECT_CALL(suggestion_line_node, background);
  EXPECT_CALL(suggestion_line_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(root_node, suggestion_line_selected);
  EXPECT_CALL(suggestion_line_selected_node, exists)
      .WillOnce(testing::Return(true));
  EXPECT_CALL(suggestion_line_selected_node, foreground);
  EXPECT_CALL(suggestion_line_selected_node, background);
  EXPECT_CALL(root_node, page_shift_active);
  EXPECT_CALL(page_shift_active_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(page_shift_active_node, foreground);
  EXPECT_CALL(page_shift_active_node, background);
  EXPECT_CALL(root_node, page_shift_inactive);
  EXPECT_CALL(page_shift_inactive_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(page_shift_inactive_node, foreground);
  EXPECT_CALL(page_shift_inactive_node, background);
  EXPECT_CALL(root_node, window);
  EXPECT_CALL(window_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(window_node, foreground);
  EXPECT_CALL(window_node, background);
  EXPECT_CALL(root_node, font);
  EXPECT_CALL(font_node, name);
  EXPECT_CALL(font_node, size);
  EXPECT_CALL(font_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(foreground_node, exists).WillRepeatedly(testing::Return(true));
  EXPECT_CALL(background_node, exists).WillRepeatedly(testing::Return(true));
  EXPECT_CALL(name_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(size_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(root_node, input_prefix_char_width);
  EXPECT_CALL(root_node, input_line_char_width);
  EXPECT_CALL(input_prefix_char_width_node, exists)
      .WillOnce(testing::Return(true));
  EXPECT_CALL(input_line_char_width_node, exists)
      .WillOnce(testing::Return(true));

  EXPECT_CALL(foreground_node, get_string)
      .WillRepeatedly(testing::Return(expected_foreground_value));
  EXPECT_CALL(background_node, get_string)
      .WillRepeatedly(testing::Return(expected_background_value));
  EXPECT_CALL(name_node, get_string)
      .WillRepeatedly(testing::Return(expected_font_name_value));
  EXPECT_CALL(size_node, get_int)
      .WillRepeatedly(testing::Return(expected_font_size_value));
  EXPECT_CALL(input_line_char_width_node, get_unsigned)
      .WillRepeatedly(testing::Return(expected_input_line_char_width));
  EXPECT_CALL(input_prefix_char_width_node, get_unsigned)
      .WillRepeatedly(testing::Return(expected_input_prefix_char_width));

  application::config::schema::UI::validate(root_node, *ui_cfg);

  EXPECT_EQ(ui_cfg->input_line_char_width, expected_input_line_char_width);
  EXPECT_EQ(ui_cfg->input_prefix_char_width, expected_input_prefix_char_width);

  EXPECT_EQ(ui_cfg->font.name, expected_font_name_value);
  EXPECT_EQ(ui_cfg->font.size, expected_font_size_value);

  EXPECT_EQ(ui_cfg->input_line.foreground, expected_foreground_value);
  EXPECT_EQ(ui_cfg->input_line.background, expected_background_value);

  EXPECT_EQ(ui_cfg->suggestion_line.foreground, expected_foreground_value);
  EXPECT_EQ(ui_cfg->suggestion_line.background, expected_background_value);

  EXPECT_EQ(ui_cfg->suggestion_line_selected.foreground,
            expected_foreground_value);
  EXPECT_EQ(ui_cfg->suggestion_line_selected.background,
            expected_background_value);

  EXPECT_EQ(ui_cfg->page_shift_active.foreground, expected_foreground_value);
  EXPECT_EQ(ui_cfg->page_shift_active.background, expected_background_value);

  EXPECT_EQ(ui_cfg->page_shift_inactive.foreground, expected_foreground_value);
  EXPECT_EQ(ui_cfg->page_shift_inactive.background, expected_background_value);

  EXPECT_EQ(ui_cfg->window.foreground, expected_foreground_value);
  EXPECT_EQ(ui_cfg->window.background, expected_background_value);
}
