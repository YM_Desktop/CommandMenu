#include "TextAndBackgroundColorsTestNode.h"

#include <ApplicationConfig.h>
#include <ApplicationConfigSchema.h>
#include <ObjectSchemaKey.h>
#include <SchemaHelpers.h>
#include <SchemaKey.h>
#include <TextAndBackgroundColors.h>
#include <array>
#include <functional>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <memory>

using namespace application::config::schema;

struct InputLineSchemaTest : public testing::Test {
  std::unique_ptr<application::ApplicationConfig> cfg;
  TextAndBackgroundColorsTestNode node;
  TextAndBackgroundColorsTestNode foreground_node, background_node;
  application::ui::TextAndBackgroundColors* input_line;

  void SetUp() override {
    cfg = std::make_unique<application::ApplicationConfig>();
    input_line = &cfg->ui_cfg.input_line;
    ON_CALL(node, GetForeground)
        .WillByDefault(testing::ReturnRef(foreground_node));
    ON_CALL(node, GetBackground)
        .WillByDefault(testing::ReturnRef(background_node));
  }

  void TearDown() override { cfg.reset(); }
};

TEST_F(InputLineSchemaTest, Validate_Foreground_and_Background_Exist) {
  const auto expected_foreground_value = std::string("#000111");
  const auto expected_background_value = std::string("#100111");
  EXPECT_CALL(node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(node, GetForeground);
  EXPECT_CALL(node, GetBackground);
  EXPECT_CALL(foreground_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(foreground_node, GetStringValue)
      .WillOnce(testing::Return(expected_foreground_value));
  EXPECT_CALL(background_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(background_node, GetStringValue)
      .WillOnce(testing::Return(expected_background_value));

  application::config::schema::INPUT_LINE::validate(node, *input_line);

  EXPECT_EQ(input_line->foreground, expected_foreground_value);
  EXPECT_EQ(input_line->background, expected_background_value);
}

TEST_F(InputLineSchemaTest,
       Validate_Foreground_DoNotExist_and_Background_Exist) {
  const auto expected_foreground_value = input_line->foreground;
  const auto expected_background_value = std::string("#100111");
  EXPECT_CALL(node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(node, GetForeground);
  EXPECT_CALL(node, GetBackground);
  EXPECT_CALL(foreground_node, exists).WillOnce(testing::Return(false));
  EXPECT_CALL(foreground_node, GetStringValue).Times(0);
  EXPECT_CALL(background_node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(background_node, GetStringValue)
      .WillOnce(testing::Return(expected_background_value));

  application::config::schema::INPUT_LINE::validate(node, *input_line);

  EXPECT_EQ(input_line->foreground, expected_foreground_value);
  EXPECT_EQ(input_line->background, expected_background_value);
}

TEST_F(InputLineSchemaTest,
       Validate_Foreground_DoNotExist_and_Background_DoNotExist) {
  const auto expected_foreground_value = input_line->foreground;
  const auto expected_background_value = input_line->background;
  EXPECT_CALL(node, exists).WillOnce(testing::Return(true));
  EXPECT_CALL(node, GetForeground);
  EXPECT_CALL(node, GetBackground);
  EXPECT_CALL(foreground_node, exists).WillOnce(testing::Return(false));
  EXPECT_CALL(foreground_node, GetStringValue).Times(0);
  EXPECT_CALL(background_node, exists).WillOnce(testing::Return(false));
  EXPECT_CALL(background_node, GetStringValue).Times(0);

  EXPECT_THROW(
      application::config::schema::INPUT_LINE::validate(node, *input_line),
      std::logic_error);

  EXPECT_EQ(input_line->foreground, expected_foreground_value);
  EXPECT_EQ(input_line->background, expected_background_value);
}
