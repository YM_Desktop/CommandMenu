#include <ApplicationConfig.h>
#include <ApplicationConfigSchema.h>
#include <ObjectSchemaKey.h>
#include <SchemaHelpers.h>
#include <SchemaKey.h>
#include <TextAndBackgroundColors.h>
#include <array>
#include <functional>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <memory>

using namespace application::config::schema;

struct ConfigSchemaTest : public testing::Test {};

TEST_F(ConfigSchemaTest, Check_BACKGROUND_schema_is_string) {
  BACKGROUND b;
  EXPECT_TRUE(b.is_value_type<std::string>());
  EXPECT_EQ(b.key(), ConfigSchemaKey::BACKGROUND);
  EXPECT_TRUE(b.required());
}

TEST_F(ConfigSchemaTest, Check_FOREGROUND_schema_is_string) {
  FOREGROUND b;
  EXPECT_TRUE(b.is_value_type<std::string>());
  EXPECT_EQ(b.key(), ConfigSchemaKey::FOREGROUND);
  EXPECT_TRUE(b.optional());
}

TEST_F(ConfigSchemaTest, Check_INPUT_MAX_LEN_schema) {
  INPUT_MAX_WIDTH imax;
  EXPECT_TRUE(imax.is_value_type<unsigned int>());
  EXPECT_EQ(imax.key(), ConfigSchemaKey::INPUT_MAX_WIDTH);
  EXPECT_TRUE(imax.required());
}
