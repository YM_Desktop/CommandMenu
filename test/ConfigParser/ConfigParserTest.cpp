#include "ConfigParserTest.h"

#include "gtest/gtest.h"
#include <ApplicationConfig.h>
#include <ConfigParser.h>
#include <memory>
#include <stdexcept>

namespace {

  std::unique_ptr<application::ApplicationConfig> expected_cfg() {
    auto cfg = std::make_unique<application::ApplicationConfig>();

    cfg->ui_cfg.input_line.foreground = "#acacac";
    cfg->ui_cfg.input_line.background = "#0c0c0c";

    cfg->ui_cfg.suggestion_line.foreground = "#acacac";
    cfg->ui_cfg.suggestion_line.background = "#0c0c0c";

    cfg->ui_cfg.suggestion_line_selected.foreground = "#00f0f0";
    cfg->ui_cfg.suggestion_line_selected.background = "#2c2c2c";

    cfg->ui_cfg.page_shift_active.foreground = "#acacac";
    cfg->ui_cfg.page_shift_active.background = "#0c0c0c";

    cfg->ui_cfg.page_shift_inactive.foreground = "#020202";
    cfg->ui_cfg.page_shift_inactive.background = "#ffffff";

    cfg->ui_cfg.window.foreground = "#acacac";
    cfg->ui_cfg.window.background = "#0c0c0c";

    cfg->ui_cfg.input_prefix_char_width = 20u;
    cfg->ui_cfg.input_prefix_char_width = 50u;

    cfg->ui_cfg.font.name = "Roboto Mono";
    cfg->ui_cfg.font.size = 10;

    cfg->menu_cfg.custom_search_paths = {"."};
    cfg->menu_cfg.use_system_path = true;

    return cfg;
  }

  std::unique_ptr<application::ApplicationConfig>
  required_keys_only_expected_cfg() {
    auto cfg = std::make_unique<application::ApplicationConfig>();

    cfg->ui_cfg.input_line.background = "#0c0c0c";

    cfg->ui_cfg.suggestion_line.background = "#0c0c0c";

    cfg->ui_cfg.suggestion_line_selected.background = "#2c2c2c";

    cfg->ui_cfg.page_shift_active.background = "#0c0c0c";

    cfg->ui_cfg.page_shift_inactive.background = "#ffffff";

    cfg->ui_cfg.window.background = "#0c0c0c";

    cfg->ui_cfg.input_line_char_width = 20u;

    cfg->ui_cfg.font.name = "Roboto Mono";

    cfg->menu_cfg.use_system_path = true;

    return cfg;
  }

  void
  configs_are_equal(const std::unique_ptr<application::ApplicationConfig>& c1,
                    const std::unique_ptr<application::ApplicationConfig>& c2) {
    const auto& ui_cfg1 = c1->ui_cfg;
    const auto& ui_cfg2 = c2->ui_cfg;

    std::array<
        std::reference_wrapper<const application::ui::TextAndBackgroundColors>,
        6ul>
        colors1 = {ui_cfg1.input_line,
                   ui_cfg1.suggestion_line,
                   ui_cfg1.suggestion_line_selected,
                   ui_cfg1.page_shift_inactive,
                   ui_cfg1.page_shift_active,
                   ui_cfg1.window};

    std::array<
        std::reference_wrapper<const application::ui::TextAndBackgroundColors>,
        6ul>
        colors2 = {ui_cfg2.input_line,
                   ui_cfg2.suggestion_line,
                   ui_cfg2.suggestion_line_selected,
                   ui_cfg2.page_shift_inactive,
                   ui_cfg2.page_shift_active,
                   ui_cfg2.window};

    static_assert(colors1.size() == colors2.size());

    for (std::size_t i = 0ul; i < colors1.size(); ++i) {
      const auto& foreground1 = colors1[i].get().foreground;
      const auto& foreground2 = colors2[i].get().foreground;
      EXPECT_EQ(foreground1, foreground2) << "Index is: " << i;

      const auto& background1 = colors1[i].get().background;
      const auto& background2 = colors2[i].get().background;
      EXPECT_EQ(background1, background2) << "Index is: " << i;
    }

    EXPECT_EQ(ui_cfg1.input_line_char_width, ui_cfg2.input_line_char_width);
    EXPECT_EQ(ui_cfg1.input_prefix_char_width, ui_cfg2.input_prefix_char_width);

    EXPECT_EQ(ui_cfg1.font.name, ui_cfg2.font.name);
    EXPECT_EQ(ui_cfg1.font.size, ui_cfg2.font.size);

    const auto& menu_cfg1 = c1->menu_cfg;
    const auto& menu_cfg2 = c2->menu_cfg;

    EXPECT_EQ(menu_cfg1.use_system_path, menu_cfg2.use_system_path);
    EXPECT_EQ(menu_cfg1.custom_search_paths, menu_cfg2.custom_search_paths);
  }

} // namespace

TEST_F(ConfigParserTest, parse_all_keys_config) {

  auto expected_config = expected_cfg();

  auto yaml_cfg =
      application::config::parsing::ConfigParser::parse(YAML_CONFIG);
  auto json_cfg =
      application::config::parsing::ConfigParser::parse(JSON_CONFIG);

  configs_are_equal(expected_config, yaml_cfg);
  configs_are_equal(expected_config, json_cfg);
}

TEST_F(ConfigParserTest, parse_required_only_keys_config) {

  auto expected_config = required_keys_only_expected_cfg();

  auto yaml_cfg = application::config::parsing::ConfigParser::parse(
      REQUIRED_ONLY_YAML_CONFIG);
  auto json_cfg = application::config::parsing::ConfigParser::parse(
      REQUIRED_ONLY_JSON_CONFIG);

  configs_are_equal(expected_config, yaml_cfg);
  configs_are_equal(expected_config, json_cfg);
}

TEST_F(ConfigParserTest, parse_required_key_missing_config) {
  EXPECT_THROW(application::config::parsing::ConfigParser::parse(
                   REQUIRED_KEY_MISSING_YAML_CONFIG),
               std::logic_error);
  EXPECT_THROW(application::config::parsing::ConfigParser::parse(
                   REQUIRED_KEY_MISSING_JSON_CONFIG),
               std::logic_error);
}

TEST_F(ConfigParserTest, parse_key_has_unexpected_type) {
  EXPECT_ANY_THROW(application::config::parsing::ConfigParser::parse(
      KEY_HAS_UNEXPECTED_TYPE_YAML_CONFIG));
  EXPECT_THROW(application::config::parsing::ConfigParser::parse(
                   KEY_HAS_UNEXPECTED_TYPE_JSON_CONFIG),
               std::logic_error);
}
