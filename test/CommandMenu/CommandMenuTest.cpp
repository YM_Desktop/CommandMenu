#include "CommandMenuTest.h"

TEST_F(CommandMenuTest, input_update_test) {
  application::cmd_menu::CommandMenuConfig cfg;
  application::cmd_menu::CommandMenu menu{cfg};
  application::cmd_menu::Suggestions actual_suggestions;

  ASSERT_TRUE(QObject::connect(
      &menu, &application::cmd_menu::CommandMenu::suggestion_page_prepared,
      [&actual_suggestions](const application::cmd_menu::Suggestions& s) {
        actual_suggestions = s;
      }));

  QString user_input;
  {
    user_input += 'p';
    menu.process_input_update(user_input);
    application::cmd_menu::Suggestions expected_suggestions{
        {   "perldoc", "dir1"},
        {   "perlivp", "dir1"},
        {"perlthanks", "dir1"},
        {    "piconv", "dir1"},
        {     "pl2pm", "dir1"},
        {  "pod2html", "dir2"},
        {   "pod2man", "dir2"},
        {  "pod2text", "dir2"},
        { "pod2usage", "dir2"},
        {"podchecker", "dir2"},
        {     "prove", "dir2"}
    };

    EXPECT_EQ(actual_suggestions, expected_suggestions);
  }

  {
    user_input += 'o';
    menu.process_input_update(user_input);
    application::cmd_menu::Suggestions expected_suggestions{
        {  "pod2html", "dir2"},
        {   "pod2man", "dir2"},
        {  "pod2text", "dir2"},
        { "pod2usage", "dir2"},
        {"podchecker", "dir2"}
    };

    EXPECT_EQ(actual_suggestions, expected_suggestions);
  }

  {
    user_input += 'd';
    menu.process_input_update(user_input);
    application::cmd_menu::Suggestions expected_suggestions{
        {  "pod2html", "dir2"},
        {   "pod2man", "dir2"},
        {  "pod2text", "dir2"},
        { "pod2usage", "dir2"},
        {"podchecker", "dir2"}
    };

    EXPECT_EQ(actual_suggestions, expected_suggestions);
  }

  {
    user_input += '2';
    menu.process_input_update(user_input);
    application::cmd_menu::Suggestions expected_suggestions{
        { "pod2html", "dir2"},
        {  "pod2man", "dir2"},
        { "pod2text", "dir2"},
        {"pod2usage", "dir2"}
    };

    EXPECT_EQ(actual_suggestions, expected_suggestions);
  }

  {
    user_input += 'm';
    menu.process_input_update(user_input);
    application::cmd_menu::Suggestions expected_suggestions{
        {"pod2man", "dir2"}
    };

    EXPECT_EQ(actual_suggestions, expected_suggestions);
  }

  {
    user_input += 'a';
    menu.process_input_update(user_input);
    application::cmd_menu::Suggestions expected_suggestions{
        {"pod2man", "dir2"}
    };

    EXPECT_EQ(actual_suggestions, expected_suggestions);
  }

  {
    user_input += 'n';
    menu.process_input_update(user_input);
    application::cmd_menu::Suggestions expected_suggestions{
        {"pod2man", "dir2"}
    };

    EXPECT_EQ(actual_suggestions, expected_suggestions);
  }
}

TEST_F(CommandMenuTest, request_suggetion_page_test) {
  application::cmd_menu::CommandMenuConfig cfg;
  application::cmd_menu::CommandMenu menu{cfg};
  application::cmd_menu::Suggestions actual_suggestions;

  ASSERT_TRUE(QObject::connect(
      &menu, &application::cmd_menu::CommandMenu::suggestion_page_prepared,
      [&actual_suggestions](const application::cmd_menu::Suggestions& s) {
        actual_suggestions = s;
      }));

  {
    QString user_input{'p'};
    menu.process_input_update(user_input);
    application::cmd_menu::Suggestions expected_suggestions{
        {   "perldoc", "dir1"},
        {   "perlivp", "dir1"},
        {"perlthanks", "dir1"},
        {    "piconv", "dir1"},
        {     "pl2pm", "dir1"},
        {  "pod2html", "dir2"},
        {   "pod2man", "dir2"},
        {  "pod2text", "dir2"},
        { "pod2usage", "dir2"},
        {"podchecker", "dir2"},
        {     "prove", "dir2"}
    };

    EXPECT_EQ(actual_suggestions, expected_suggestions);
  }
  menu.suggestion_page_requested(2, 4);
  {
    application::cmd_menu::Suggestions expected_suggestions{
        {"perlthanks", "dir1"},
        {    "piconv", "dir1"},
        {     "pl2pm", "dir1"},
        {  "pod2html", "dir2"}
    };

    EXPECT_EQ(actual_suggestions, expected_suggestions);
  }
  // Check whether count overlaps real capacity.
  // In this case page returns everything till end.
  menu.suggestion_page_requested(2, 200);
  {
    application::cmd_menu::Suggestions expected_suggestions{
        {"perlthanks", "dir1"},
        {    "piconv", "dir1"},
        {     "pl2pm", "dir1"},
        {  "pod2html", "dir2"},
        {   "pod2man", "dir2"},
        {  "pod2text", "dir2"},
        { "pod2usage", "dir2"},
        {"podchecker", "dir2"},
        {     "prove", "dir2"}
    };

    EXPECT_EQ(actual_suggestions, expected_suggestions);
  }
}
