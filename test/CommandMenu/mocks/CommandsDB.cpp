#include <CommandsDB.h>

namespace {
  application::cmd_menu::CommandsBinariesMap test_map{
      {"dir1",
       {
       "perldoc",
       "perlivp",
       "perlthanks",
       "piconv",
       "pl2pm",
       }},
      {
       "dir2", {
 "pod2html",
 "pod2man",
 "pod2text",
 "pod2usage",
 "podchecker",
 "prove",
 "streamzip",
 "xsubpp",
 "zipdetails",
 }, }
  };
}

namespace application::cmd_menu {

  CommandsDB::CommandsDB(const CommandMenuConfig& cfg)
      : m_binaries(test_map) {}

  CommandsDB::~CommandsDB() = default;

  const CommandsBinariesMap& CommandsDB::binaries() const noexcept {
    return m_binaries;
  }
} // namespace application::cmd_menu

