#include "SearchEngineTest.h"

TEST_F(SearchEngineTest, push_pop_a) {
  application::cmd_menu::SearchEngine engine{cfg};
  application::cmd_menu::Suggestions expected_suggestions{
      {"another file", "dir1"}
  };

  {
    auto suggestions = engine.push('a');
    EXPECT_TRUE(suggestions.has_value());

    const auto& actual_suggestions = suggestions->get();
    EXPECT_EQ(expected_suggestions, actual_suggestions);
  }

  {
    auto suggestions = engine.pop();
    EXPECT_FALSE(suggestions.has_value());
  }
}

TEST_F(SearchEngineTest, push_pop_file) {
  application::cmd_menu::SearchEngine engine{cfg};
  const std::map<char, application::cmd_menu::Suggestions>
      expected_suggestions_by_char{
          {'f',
           {{
           "file1",
           "dir1",
           },
           {
           "file2",
           "dir1",
           },
           {
           "file3",
           "dir1",
           },
           {
           "file4",
           "dir1",
           },
           {
           "file5",
           "dir1",
           },
           {"flower", "dir2"}}     },
          {'i',
           {{"file1", "dir1"},
           {"file2", "dir1"},
           {"file3", "dir1"},
           {"file4", "dir1"},
           {"file5", "dir1"}}      },
          {'l',
           {{"file1", "dir1"},
           {"file2", "dir1"},
           {"file3", "dir1"},
           {"file4", "dir1"},
           {"file5", "dir1"}}      },
          {'e',
           {{"file1", "dir1"},
           {"file2", "dir1"},
           {"file3", "dir1"},
           {"file4", "dir1"},
           {"file5", "dir1"}}      },
          {'5', {{"file5", "dir1"}}},
  };

  // Immitate user inputs 'file5' char-by-char
  for (const auto c : {'f', 'i', 'l', 'e', '5'}) {
    auto suggestions = engine.push(c);
    EXPECT_TRUE(suggestions.has_value());

    const auto& actual_suggestions = suggestions->get();
    EXPECT_EQ(expected_suggestions_by_char.at(c), actual_suggestions);
  }

  {
    auto last_suggestions = engine.suggestions();
    EXPECT_TRUE(last_suggestions);
    EXPECT_EQ(expected_suggestions_by_char.at('5'), last_suggestions->get());
  }

  // Immitate user started to delete each char from the end.
  // '5' is already deleted, so suggestions check must be from
  // 'e'.
  for (const auto c : {'e', 'l', 'i', 'f'}) {

    auto suggestions = engine.pop();
    EXPECT_TRUE(suggestions.has_value());

    const auto& actual_suggestions = suggestions->get();
    EXPECT_EQ(expected_suggestions_by_char.at(c), actual_suggestions);
  }

  // Check whether nothing else left in engine
  auto suggestions = engine.pop();
  EXPECT_FALSE(suggestions.has_value());
}

TEST_F(SearchEngineTest, clear_after_push) {
  application::cmd_menu::SearchEngine engine{cfg};
  application::cmd_menu::Suggestions expected_suggestions{
      {"another file", "dir1"}
  };

  {
    auto suggestions = engine.push('a');
    EXPECT_TRUE(suggestions.has_value());

    const auto& actual_suggestions = suggestions->get();
    EXPECT_EQ(expected_suggestions, actual_suggestions);
  }

  {
    engine.clear();
    auto suggestions = engine.suggestions();
    EXPECT_FALSE(suggestions.has_value());
  }
}

TEST_F(SearchEngineTest, push_till_nonexisting_and_pop) {
  application::cmd_menu::SearchEngine engine{cfg};
  application::cmd_menu::Suggestions expected_suggestions{
      {"another file", "dir1"}
  };

  {
    engine.push('a');
    auto suggestions = engine.push('d');
    EXPECT_FALSE(suggestions.has_value());
  }

  {
    auto suggestions = engine.pop();
    EXPECT_TRUE(suggestions.has_value());
  }
}
