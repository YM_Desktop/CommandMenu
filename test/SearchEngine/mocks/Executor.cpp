#include "Executor.h"

namespace application::cmd_menu {

  Executor::Executor() = default;
  Executor::~Executor() = default;

  void Executor::execute(std::string&& cmd) const noexcept {}
} // namespace application::cmd_menu

