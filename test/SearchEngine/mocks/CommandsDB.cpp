#include "CommandsDB.h"

namespace {
  application::cmd_menu::CommandsBinariesMap test_map{
      {"dir1",
       {"file1", "file2", "file3", "file4", "file5", "another file",
       "some other file", "link"}},
      {"dir2",
       {
       "corelist",
       "cpan",
       "enc2xs",
       "encguess",
       "flower",
       "h2ph",
       "h2xs",
       "instmodsh",
       "json_pp",
       "libnetcfg",
       "perlbug",
       "perldoc",
       "perlivp",
       "perlthanks",
       "piconv",
       "pl2pm",
       }},
      {
       "dir3",                {
                "pod2html",
                "pod2man",
                "pod2text",
                "pod2usage",
                "podchecker",
                "prove",
                "ptar",
                "ptardiff",
                "ptargrep",
                "shasum",
                "splain",
                "streamzip",
                "xsubpp",
                "zipdetails",
                }, }
  };
}

namespace application::cmd_menu {

  CommandsDB::CommandsDB(const CommandMenuConfig& cfg)
      : m_binaries(test_map) {}

  CommandsDB::~CommandsDB() = default;

  const CommandsBinariesMap& CommandsDB::binaries() const noexcept {
    return m_binaries;
  }
} // namespace application::cmd_menu

