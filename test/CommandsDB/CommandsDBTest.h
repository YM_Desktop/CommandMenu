#pragma once
#include <CommandMenuConfig.h>
#include <CommandsDB.h>
#include <filesystem>
#include <fstream>
#include <gtest/gtest.h>
#include <list>
#include <vector>

struct CommandsDBTest : public testing::Test {

  std::list<std::filesystem::path> parse_env_path_to_paths() noexcept;

  std::vector<std::filesystem::path> create_test_dirs();

  std::vector<std::filesystem::path> m_test_dirs;

  void SetUp() override;
  void TearDown() override;
};
