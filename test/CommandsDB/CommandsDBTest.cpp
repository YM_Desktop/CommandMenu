#include "CommandsDBTest.h"

std::list<std::filesystem::path>
CommandsDBTest::parse_env_path_to_paths() noexcept {

  std::list<std::filesystem::path> paths;

  if (std::string env_path = std::getenv("PATH"); !env_path.empty()) {

    std::string_view env_path_view = env_path;
    std::size_t start_index{0ul};
    std::size_t sub_index{env_path_view.find_first_of(':')};

    do {
      paths.emplace_back(
          env_path_view.substr(start_index, sub_index - start_index));
      start_index = sub_index + 1;
      sub_index = env_path_view.find_first_of(':', start_index);
    } while (sub_index != std::string::npos);

    if (start_index != std::string::npos) {
      paths.emplace_back(env_path_view.substr(start_index));
    }
  }

  return paths;
}

std::vector<std::filesystem::path> CommandsDBTest::create_test_dirs() {
  const auto temp_dir = std::filesystem::temp_directory_path();
  std::vector<std::filesystem::path> paths{
      std::filesystem::path(temp_dir / std::filesystem::path("dir1")),
      std::filesystem::path(temp_dir / std::filesystem::path("dir2"))};

  {
    std::filesystem::create_directory(paths.front());
    std::ofstream(paths.front() / std::filesystem::path("file1")).close();
    std::filesystem::permissions(paths.front() / std::filesystem::path("file1"),
                                 std::filesystem::perms::owner_exec,
                                 std::filesystem::perm_options::add);
    std::ofstream(paths.front() / std::filesystem::path("abcd")).close();
    std::filesystem::permissions(paths.front() / std::filesystem::path("abcd"),
                                 std::filesystem::perms::owner_exec,
                                 std::filesystem::perm_options::add);
  }
  {
    std::filesystem::create_directory(paths.back());
    std::ofstream(paths.back() / std::filesystem::path("non-exec")).close();
    std::ofstream(paths.back() / std::filesystem::path("exec")).close();
    std::filesystem::permissions(paths.back() / std::filesystem::path("exec"),
                                 std::filesystem::perms::owner_exec,
                                 std::filesystem::perm_options::add);
  }

  return paths;
}

void CommandsDBTest::SetUp() { m_test_dirs = create_test_dirs(); }

void CommandsDBTest::TearDown() {
  for (const auto& dir : m_test_dirs) {
    ASSERT_TRUE(std::filesystem::remove_all(dir));
  }
}

TEST_F(CommandsDBTest, create_database_with_custom_paths_without_system) {
  application::cmd_menu::CommandMenuConfig cfg{
      .custom_search_paths = m_test_dirs, .use_system_path = false};

  const auto temp_dir = std::filesystem::temp_directory_path();
  const application::cmd_menu::CommandsBinariesMap expected_binaries{
      {std::filesystem::path(temp_dir / std::filesystem::path("dir1")).c_str(),
       {"abcd", "file1"}},
      {std::filesystem::path(temp_dir / std::filesystem::path("dir2")).c_str(),
       {"exec"}         }
  };

  application::cmd_menu::CommandsDB db{cfg};
  const auto& binaries = db.binaries();
  EXPECT_EQ(expected_binaries, binaries);
}

TEST_F(CommandsDBTest, create_database_without_custom_and_system_paths) {
  application::cmd_menu::CommandMenuConfig cfg{.use_system_path = false};

  application::cmd_menu::CommandsDB db{cfg};
  const auto& binaries = db.binaries();
  EXPECT_TRUE(binaries.empty());
}

TEST_F(CommandsDBTest, create_database_with_system_paths_only) {
  application::cmd_menu::CommandMenuConfig cfg{.custom_search_paths = {},
                                               .use_system_path = true};

  application::cmd_menu::CommandsDB db{cfg};
  const auto& binaries = db.binaries();
  EXPECT_TRUE(!binaries.empty());

  auto env_paths = parse_env_path_to_paths();
  for (const auto& p : env_paths) {
    EXPECT_TRUE(binaries.contains(p.string()));
  }
}

TEST_F(CommandsDBTest, create_database_with_system_and_custom_paths) {
  application::cmd_menu::CommandMenuConfig cfg{
      .custom_search_paths = m_test_dirs, .use_system_path = true};

  application::cmd_menu::CommandsDB db{cfg};
  const auto& binaries = db.binaries();
  EXPECT_TRUE(!binaries.empty());

  auto env_paths = parse_env_path_to_paths();
  for (const auto& p : env_paths) {
    EXPECT_TRUE(binaries.contains(p.string()));
  }

  for (const auto& p : m_test_dirs) {
    EXPECT_TRUE(binaries.contains(p.string()));
  }
}
