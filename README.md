# CommandMenu

## Description
Simple interactive command menu for linux systems, used for active search-and-execute workflow.

## Dependencies
- cmake v3.20 - build system.
- qt6 - graphical library for UI display.
- clang v13 - compiler
- Doxygen - code documentation tool (Debug build only)
- QTest - unit tests library (part of qt6, Debug build only)

## Build
To build application for regular user next commands must be used (Release build):
- cmake -S . -B build -DCMAKE_BUILD_TYPE=Release
- cmake --build build -- -j `nproc`
- sudo cmake --install build

To build application for development mode use next commands (Debug build):
- cmake -S . -B build -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=build
- cmake --build build -- -j `nproc`
- cmake --install build

## Run
In case of successful __Release__ build binary should be in user *PATH*.
This means that you need just to execute `./CommandMenu`.

In case of successful __Debug__ build binary must be found under `${CMAKE_INSTALL_PREFIX}/bin` folder.
It can be launched by `./${CMAKE_INSTALL_PREFIX}/bin/CommandMenu`.

## Tests (Debug mode only)
To run unit tests next command can be used:
`ctest --test-dir build/test`

If you want to make tests more verbose try using:
`ctest --test-dir build/test --verbose`
