cmake_minimum_required(VERSION 3.20)

set(CMAKE_C_COMPILER clang)
set(CMAKE_CXX_COMPILER clang++)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)
set(CMAKE_EXPORT_COMPILE_COMMANDS TRUE)
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

enable_language(CXX)

if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
	list(APPEND QT_COMPONENTS_REQUIRED Core Widgets Test)
else()
	list(APPEND QT_COMPONENTS_REQUIRED Core Widgets)
endif()

find_package(Qt6 REQUIRED COMPONENTS ${QT_COMPONENTS_REQUIRED})
include_directories(${QT_USE_FILE})

include(${CMAKE_SOURCE_DIR}/yaml-cpp.cmake)

project(CommandMenu)

add_subdirectory(src)

if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
	add_subdirectory(test)
endif()

install(TARGETS ${PROJECT_NAME})
