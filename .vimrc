command! Init  :!cmake -S . -B build -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=build
command! Build :!cmake --build build -- -j `nproc` && cmake --install build
command! Test  :!LD_LIBRARY_PATH=${PWD}/build/lib ctest --test-dir build/test
command! TestV :!LD_LIBRARY_PATH=${PWD}/build/lib ctest --test-dir build/test --verbose
command! Run   :!./build/bin/CommandMenu
command! CBuild :!cmake --build build --target clean && cmake --build build -- -j `nproc` && cmake --install build
command! -nargs=1 TBuild :!cmake --build build --target <f-args> -j `nproc` 
command! -nargs=1 TTest :!LD_LIBRARY_PATH=${PWD}/build/lib ctest --test-dir build/test -R <f-args>
command! -nargs=1 TTestV :!LD_LIBRARY_PATH=${PWD}/build/lib ctest --test-dir build/test -R <f-args> --verbose
