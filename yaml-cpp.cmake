set(YAML_DIR ${CMAKE_BINARY_DIR}/yaml-cpp)
set(YAML_INCLUDE_DIR ${YAML_DIR}/include)
set(YAML_SHARED_LIB ${CMAKE_INSTALL_PREFIX}/lib/libyaml-cpp.so)
set(YAML_REPO https://github.com/jbeder/yaml-cpp.git)

if(NOT EXISTS ${YAML_DIR})
	add_custom_command(OUTPUT ${YAML_DIR} ${YAML_INCLUDE_DIR}
		COMMAND git clone ${YAML_REPO}
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
	)
endif()

add_custom_command(OUTPUT ${YAML_SHARED_LIB}
	COMMAND cmake -S . -B build -DYAML_BUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX}
	COMMAND cmake --build build -- -j `nproc`
	COMMAND cmake --install build
	DEPENDS ${YAML_DIR}
	WORKING_DIRECTORY ${YAML_DIR}
)

add_custom_target(yaml_cpp DEPENDS 
	${YAML_DIR}
	${YAML_SHARED_LIB}
)
