#include "SuggestionLine.h"

#include <Config.h>
#include <CustomLineEdit.h>
#include <SuggestionLineItem.h>

namespace application::ui {
  SuggestionLine::SuggestionLine(const Config& cfg)
      : QWidget()
      , m_page(0, 0) {
    setLayout(&m_layout);
    m_regular_palette.setColor(QPalette::Text,
                               QColor(cfg.suggestion_line.foreground.c_str()));
    m_regular_palette.setColor(QPalette::Base,
                               QColor(cfg.suggestion_line.background.c_str()));
    m_selected_palette.setColor(
        QPalette::Text,
        QColor(cfg.suggestion_line_selected.foreground.c_str()));
    m_selected_palette.setColor(
        QPalette::Base,
        QColor(cfg.suggestion_line_selected.background.c_str()));
  }
  SuggestionLine::~SuggestionLine() = default;

  void SuggestionLine::update_suggestions(
      const application::cmd_menu::Suggestions& suggestions) {
    static const auto font_metrics = fontMetrics();
    static const auto max_char_width = fontMetrics().maxWidth();

    m_suggestions.clear();
    if (suggestions.empty())
      return;

    auto& [_, items_displayed] = m_page;
    items_displayed = 0;
    int summary_graphical_width = 0;
    for (const auto& s : suggestions) {

      auto suggestion_graphical_width =
          font_metrics.tightBoundingRect(s.binary.data()).width() +
          max_char_width;
      summary_graphical_width +=
          suggestion_graphical_width + m_layout.spacing();
      if (summary_graphical_width > width()) {
        break;
      }

      auto& item = m_suggestions.emplace_back(new SuggestionLineItem(
          s, suggestion_graphical_width + max_char_width));
      item->setPalette(m_regular_palette);
      m_layout.addWidget(item.get());
      ++items_displayed;
    }

    m_selected_suggestion = m_suggestions.begin();
    m_suggestions.front()->setPalette(m_selected_palette);

    emit current_suggestion_updated(m_suggestions.front());
    emit current_position_updated(current_position());
  }

  void SuggestionLine::select_next() noexcept {
    if (m_suggestions.empty())
      return;

    if (auto next_suggestion = std::next(m_selected_suggestion);
        next_suggestion != m_suggestions.end()) {

      m_selected_suggestion->get()->setPalette(m_regular_palette);
      m_selected_suggestion = next_suggestion;
      m_selected_suggestion->get()->setPalette(m_selected_palette);

      emit current_suggestion_updated(*m_selected_suggestion);
      emit current_position_updated(current_position());
    }
  }

  void SuggestionLine::select_prev() noexcept {
    if (m_suggestions.empty())
      return;

    if (m_selected_suggestion == m_suggestions.begin())
      return;

    if (m_selected_suggestion != m_suggestions.begin()) {

      m_selected_suggestion->get()->setPalette(m_regular_palette);
      m_selected_suggestion = std::prev(m_selected_suggestion);
      m_selected_suggestion->get()->setPalette(m_selected_palette);

      emit current_suggestion_updated(*m_selected_suggestion);
      emit current_position_updated(current_position());
    }
  }

  void SuggestionLine::clear() noexcept {
    m_selected_suggestion = m_suggestions.begin();
    m_suggestions.clear();
    m_page = {0, 0};
    emit current_suggestion_updated(nullptr);
    emit current_position_updated(current_position());
  }

  void SuggestionLine::submit_selection() noexcept {
    if (!m_suggestions.empty())
      emit suggestion_selected(m_selected_suggestion->get()->text());
  }

  void SuggestionLine::request_prev_page() noexcept {
    auto& [start_item, items_count] = m_page;
    if (start_item == 0)
      return;

    if (start_item <= items_count)
      start_item = 0u;
    else
      start_item = start_item - items_count;

    // When previous page is requested it is worthy to
    // fill the suggestion line till the end, e.g. by providing
    // twice of items_count.
    suggestion_page_requested(start_item, items_count * 2);
  }

  void SuggestionLine::request_next_page() noexcept {
    auto& [start_item, items_count] = m_page;
    start_item = start_item + items_count;
    suggestion_page_requested(start_item, items_count);
  }

  void SuggestionLine::suggestions_page_rejected() {
    auto& [start_item, items_count] = m_page;
    start_item = start_item - items_count;
  }

  unsigned int SuggestionLine::current_position() const noexcept {
    std::vector<std::shared_ptr<SuggestionLineItem>>::const_iterator
        suggestions_start = m_suggestions.begin();

    std::vector<std::shared_ptr<SuggestionLineItem>>::const_iterator
        current_suggestion = m_selected_suggestion;

    unsigned int distance =
        std::distance(suggestions_start, current_suggestion);

    return m_page.first + distance + 1u;
  }
} // namespace application::ui
