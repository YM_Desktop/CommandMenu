#pragma once
#include <CustomLayout.h>
#include <QPalette>
#include <QWidget>
#include <Suggestions.h>
#include <memory>
#include <vector>

namespace application::ui {
  class SuggestionLineItem;
}

namespace application::ui {

  class CustomLineEdit;
  struct Config;

  /**
   * @class SuggestionLine
   * UI representation of suggestions provided to user by search results.
   */
  class SuggestionLine : public QWidget {
    Q_OBJECT

    /**
     * @brief List of suggestions labels.
     */
    std::vector<std::shared_ptr<SuggestionLineItem>> m_suggestions;

    /**
     * @brief Current selected suggestion.
     */
    std::vector<std::shared_ptr<SuggestionLineItem>>::iterator
        m_selected_suggestion;

    /**
     * @brief Layout instance.
     */
    CustomLayout m_layout;

    /**
     * @brief Not selected suggestion label palette.
     */
    QPalette m_regular_palette;

    /**
     * @brief Selected suggestion label palette.
     */
    QPalette m_selected_palette;

    /**
     * @brief Current search results page info.
     */
    std::pair<unsigned int, unsigned int> m_page;

  public:
    /**
     * @fn SuggestionLine
     * @brief Constructor.
     * @param cfg: UI config
     */
    SuggestionLine(const Config& cfg);

    /**
     * @fn ~SuggestionLine
     * @brief Destructor.
     */
    ~SuggestionLine();

    /**
     * @fn application::ui::SuggestionLine::update_suggestions
     * Updates current suggestions line. If suggestions line exceeds
     * container width - cuts of other suggestions provided in param.
     * @param suggestions: Suggestions to apply.
     */
    void
    update_suggestions(const application::cmd_menu::Suggestions& suggestions);

    /**
     * @fn application::ui::SuggestionLine::select_next
     * @brief Makes next suggestion selected in current page.
     */
    void select_next() noexcept;

    /**
     * @fn application::ui::SuggestionLine::select_prev
     * @brief Makes prev suggestion selected in current page.
     */
    void select_prev() noexcept;

    /**
     * @fn application::ui::SuggestionLine::clear
     * @brief Clear all suggestions.
     */
    void clear() noexcept;

    /**
     * @fn application::ui::SuggestionLine::submit_selection
     * @brief emits suggestion_selected signal with current selected suggestion.
     */
    void submit_selection() noexcept;

    /**
     * @fn application::ui::SuggestionLine::request_prev_page
     * @brief requests previous suggestion page.
     */
    void request_prev_page() noexcept;

    /**
     * @fn application::ui::SuggestionLine::request_next_page
     * @brief requests next suggestion page.
     */
    void request_next_page() noexcept;

    /**
     * @fn application::ui::SuggestionLine::suggestions_page_rejected
     * @brief reverts requested page info to those before request.
     */
    void suggestions_page_rejected();

    /**
     * @fn application::ui::SuggestionLine::current_position
     * @brief Returns position of currently selected suggestion.
     * @return unsigned int
     */
    inline unsigned int current_position() const noexcept;

  signals:

    /**
     * @fn application::ui::SuggestionLine::suggestion_selected
     * @brief signal. Notifies that suggestion has been selected.
     */
    void suggestion_selected(const QString&);

    /**
     * @fn application::ui::SuggestionLine::suggestion_page_requested
     * @brief signal. Notifies that suggestion has been selected.
     */
    void suggestion_page_requested(unsigned int, unsigned int);

    /**
     * @fn application::ui::SuggestionLine::current_suggestion_updated
     * @brief signal. Notifies whether user visually selected suggestion.
     */
    void current_suggestion_updated(std::shared_ptr<SuggestionLineItem>);

    /**
     * @fn application::ui::SuggestionLine::current_position_updated
     * @brief signal. Notifies current position of selected suggestion updated.
     */
    void current_position_updated(unsigned int);
  };
} // namespace application::ui
