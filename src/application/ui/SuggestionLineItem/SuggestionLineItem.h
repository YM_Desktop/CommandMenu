#pragma once
#include <CustomLineEdit.h>
#include <string_view>

namespace application::cmd_menu {
  struct Suggestion;
}

namespace application::ui {
  /**
   * @class SuggestionLineItem
   * @brief UI representation of suggestion.
   */
  class SuggestionLineItem : public CustomLineEdit {

    /**
     * @brief suggestion prefix.
     */
    std::string_view m_prefix;

  public:
    /**
     * @fn SuggestionLineItem
     * @brief Constructor.
     * @param suggestion
     * @param max_width: maximum graphical width used by this item.
     */
    SuggestionLineItem(const cmd_menu::Suggestion& suggestion, int max_width);

    /**
     * @fn ~SuggestionLineItem
     * @brief Destructor.
     */
    ~SuggestionLineItem();

    /**
     * @fn prefix
     * @brief provides access to suggestion prefix.
     * @return string_view of prefix.
     */
    std::string_view prefix() const noexcept;
  };
} // namespace application::ui
