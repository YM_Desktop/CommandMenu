#include "SuggestionLineItem.h"

#include <Suggestions.h>

namespace application::ui {
  SuggestionLineItem::SuggestionLineItem(const cmd_menu::Suggestion& suggestion,
                                         int max_width)
      : CustomLineEdit()
      , m_prefix(suggestion.prefix) {

    setText(suggestion.binary.data());
    setMaximumWidth(max_width);
    setAlignment(Qt::AlignBaseline | Qt::AlignCenter);
  }

  SuggestionLineItem::~SuggestionLineItem() = default;

  std::string_view SuggestionLineItem::prefix() const noexcept {
    return m_prefix;
  }
} // namespace application::ui
