#pragma once
#include <CustomLineEdit.h>

namespace application::ui {
  struct Config;

  /**
   * @class InputLine
   * @brief Handles user input.
   */
  class InputLine : public CustomLineEdit {

  public:
    /**
     * @fn InputLine
     * @brief Constructor.
     * @param cfg : application::ui::Config
     */
    InputLine(const Config& cfg);

    /**
     * @fn ~InputLine
     * @brief Destructor.
     */
    ~InputLine();

    /**
     * @fn handle_key_press
     * @brief Handles user key pressed events.
     * @param event: QKeyEvent
     */
    void handle_key_press(QKeyEvent* event);

    /**
     * @fn clear
     * @brief Clears all chars from input.
     */
    void clear();

    /**
     * @fn remove_char
     * @brief Removes last inputted char.
     */
    void remove_char() noexcept;
  };
} // namespace application::ui
