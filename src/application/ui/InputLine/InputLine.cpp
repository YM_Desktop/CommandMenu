#include "InputLine.h"

#include <Config.h>
#include <QPalette>

namespace application::ui {
  InputLine::InputLine(const Config& cfg)
      : CustomLineEdit() {
    QPalette p;
    p.setColor(foregroundRole(), QColor(cfg.input_line.foreground.c_str()));
    p.setColor(backgroundRole(), QColor(cfg.input_line.background.c_str()));
    setPalette(p);
  }

  InputLine::~InputLine() = default;

  void InputLine::handle_key_press(QKeyEvent* event) {
    setReadOnly(false);
    CustomLineEdit::keyPressEvent(event);
    setReadOnly(true);
  }

  void InputLine::clear() {
    selectAll();
    del();
  }

  void InputLine::remove_char() noexcept { backspace(); }
} // namespace application::ui
