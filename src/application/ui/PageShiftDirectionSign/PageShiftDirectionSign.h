#pragma once
#include <QLabel>
#include <QPalette>

namespace application::ui {

  struct Config;

  /**
   * @class PageShiftDirectionSign
   * @brief Represents user-defined page shift direction hint.
   */
  class PageShiftDirectionSign : public QLabel {
    Q_OBJECT

    /**
     * @brief Active palette
     */
    QPalette m_active_palette;

    /**
     * @brief Inactive palette
     */
    QPalette m_inactive_palette;

  public:
    /**
     * @fn PageShiftDirectionSign
     * @brief Constructor
     * @param cfg: ui configuration
     * @param sign: user-defined sign for page shift direction.
     */
    PageShiftDirectionSign(const Config& cfg, const QString& sign);

    /**
     * @fn ~PageShiftDirectionSign
     * @brief Destructor
     */
    ~PageShiftDirectionSign();

    /**
     * @fn set_active
     * @brief toggles current palette accordingly to param
     * @param active: should be displayed as active color or not.
     */
    void set_active(bool active) noexcept;
  };
} // namespace application::ui
