#include "PageShiftDirectionSign.h"

#include <Config.h>
#include <QPalette>

namespace application::ui {
  PageShiftDirectionSign::PageShiftDirectionSign(const Config& cfg,
                                                 const QString& sign)
      : QLabel(sign) {
    setAutoFillBackground(true);
    setFixedWidth(fontMetrics().maxWidth());
    setAlignment(Qt::AlignCenter);

    m_inactive_palette.setColor(
        foregroundRole(), QColor(cfg.page_shift_inactive.foreground.c_str()));
    m_inactive_palette.setColor(
        backgroundRole(), QColor(cfg.page_shift_inactive.background.c_str()));
    m_active_palette.setColor(foregroundRole(),
                              QColor(cfg.page_shift_active.foreground.c_str()));
    m_active_palette.setColor(backgroundRole(),
                              QColor(cfg.page_shift_active.background.c_str()));
    set_active(false);
  }

  PageShiftDirectionSign::~PageShiftDirectionSign() = default;

  void PageShiftDirectionSign::set_active(bool active) noexcept {
    if (active) {
      setPalette(m_active_palette);
      return;
    }

    setPalette(m_inactive_palette);
  }
} // namespace application::ui
