#pragma once

#include <QHBoxLayout>

namespace application::ui {
  /**
   * @class CustomLayout
   * @brief Horizontal layout with small modifications.
   */
  class CustomLayout : public QHBoxLayout {
  public:
    /**
     * @fn CustomLayout
     * @brief Constructor.
     */
    CustomLayout();

    /**
     * @fn ~CustomLayout
     * @brief Destructor.
     */
    ~CustomLayout();
  };
} // namespace application::ui
