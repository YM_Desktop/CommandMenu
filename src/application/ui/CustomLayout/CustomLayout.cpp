#include "CustomLayout.h"

namespace application::ui {
  CustomLayout::CustomLayout()
      : QHBoxLayout() {
    setSpacing(10);
    setContentsMargins(0, 0, 0, 0);
    setAlignment(Qt::AlignBaseline | Qt::AlignLeft | Qt::AlignVCenter);
  }

  CustomLayout::~CustomLayout() = default;
} // namespace application::ui
