#include "SuggestionCounter.h"

#include <Config.h>

namespace application::ui {

  SuggestionCounter::SuggestionCounter(const Config& cfg)
      : CustomLineEdit()
      , m_count(0ul) {

    QPalette p;
    p.setColor(foregroundRole(), QColor(cfg.input_line.foreground.c_str()));
    p.setColor(backgroundRole(), QColor(cfg.input_line.background.c_str()));
    setPalette(p);

    setText("0 / 0");
  }

  SuggestionCounter::~SuggestionCounter() = default;

  void SuggestionCounter::current_position_updated(unsigned int position) {
    setText(QString("%1 / %2").arg(position).arg(m_count));
  }

  void SuggestionCounter::set_count(std::size_t suggestions_count) {
    m_count = suggestions_count;
    setText(QString("%1 / %2").arg(m_count ? 1u : 0u).arg(m_count));
  }
} // namespace application::ui
