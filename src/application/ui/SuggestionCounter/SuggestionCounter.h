#pragma once
#include <CustomLineEdit.h>

namespace application::ui {

  struct Config;

  /**
   * @class SuggestionCounter
   * UI representation of suggestions counter
   */
  class SuggestionCounter : public CustomLineEdit {
    Q_OBJECT

    /**
     * @brief count of last suggestions.
     */
    std::size_t m_count;

  public:
    /**
     * @fn SuggestionCounter
     * @brief Constructor.
     * @param cfg: UI config
     */
    SuggestionCounter(const Config& cfg);

    /**
     * @fn ~SuggestionCounter
     * @brief Destructor.
     */
    ~SuggestionCounter();

    /**
     * @fn current_position_updated
     * @brief Saves current position and updates current text.
     * @param position
     */
    void current_position_updated(unsigned int position);

    /**
     * @fn set_count
     * @brief Saves current number of suggestions.
     * @param suggestions_count
     */
    void set_count(std::size_t suggestions_count);
  };
} // namespace application::ui
