#pragma once
#include <CustomLineEdit.h>

namespace application::ui {
  class SuggestionLineItem;
}

namespace application::ui {
  struct Config;

  /**
   * @class InputPrefix
   * @brief Shows prefix to suggestion.
   */
  class InputPrefix : public CustomLineEdit {

  public:
    /**
     * @fn InputPrefix
     * @brief Constructor.
     * @param cfg : application::ui::Config
     */
    InputPrefix(const Config& cfg);

    /**
     * @fn ~InputPrefix
     * @brief Destructor.
     */
    ~InputPrefix();

    /**
     * @fn update_prefix
     * @brief Updates current prefix for current suggestion.
     * @param suggestion_item
     */
    void update_prefix(std::shared_ptr<SuggestionLineItem> suggestion_item);
  };
} // namespace application::ui
