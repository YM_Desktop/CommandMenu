#include "InputPrefix.h"

#include <Config.h>
#include <QPalette>
#include <SuggestionLineItem.h>

namespace application::ui {
  InputPrefix::InputPrefix(const Config& cfg)
      : CustomLineEdit() {
    QPalette p;
    p.setColor(foregroundRole(), QColor(cfg.input_line.foreground.c_str()));
    p.setColor(backgroundRole(), QColor(cfg.input_line.background.c_str()));
    setPalette(p);
  }

  InputPrefix::~InputPrefix() = default;

  void InputPrefix::update_prefix(
      std::shared_ptr<SuggestionLineItem> suggestion_item) {
    setReadOnly(false);
    setText(suggestion_item ? suggestion_item->prefix().data() : "");
    setReadOnly(true);
  }
} // namespace application::ui
