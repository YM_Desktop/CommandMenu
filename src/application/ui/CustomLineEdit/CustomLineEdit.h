#pragma once
#include <QLineEdit>

namespace application::ui {
  /**
   * @class CustomLineEdit
   * @brief Base class for text labels with modifications.
   */
  class CustomLineEdit : public QLineEdit {
  public:
    /**
     * @fn CustomLineEdit
     * @brief Constructor.
     */
    CustomLineEdit();

    /**
     * @fn ~CustomLineEdit
     * @brief Destructor.
     */
    virtual ~CustomLineEdit();
  };
} // namespace application::ui
