#include "CustomLineEdit.h"

#include <QLineEdit>
#include <QPalette>

namespace application::ui {
  CustomLineEdit::CustomLineEdit()
      : QLineEdit() {
    setAutoFillBackground(true);
    setContentsMargins(0, 0, 0, 0);
    setReadOnly(true);
    setFrame(false);
    setAlignment(Qt::AlignBaseline | Qt::AlignLeft);

    QPalette p;
    p.setColor(QPalette::Base, Qt::transparent);
    setPalette(p);
  }

  CustomLineEdit::~CustomLineEdit() = default;
} // namespace application::ui
