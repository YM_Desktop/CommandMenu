#include "CommandMenuWindow.h"

#include "InputLine.h"

#include <Config.h>
#include <QApplication>
#include <QFont>
#include <QKeyEvent>
#include <QPalette>
#include <QScreen>
#include <QSize>
#include <UserAction.h>

namespace {
  application::ui::UserAction
  key_event_to_action(QKeyEvent* event,
                      const application::ui::KeyMappings& mappings) noexcept {
    auto key = event->key();
    auto modifiers = event->modifiers();
    for (const auto& mapping : mappings) {
      if (key == mapping.key && modifiers == mapping.modifier)
        return mapping.action;
    }

    return application::ui::UserAction::NONE;
  }
} // namespace

namespace application::ui {
  CommandMenuWindow::CommandMenuWindow(const Config& cfg)
      : m_input_prefix(cfg)
      , m_input_line(cfg)
      , m_suggestion_line(cfg)
      , m_key_mappings(std::move(cfg.key_mappings))
      , m_left_page_sign(cfg, "<")
      , m_right_page_sign(cfg, ">")
      , m_suggestions_counter(cfg) {

    setFocus();

    QPalette p;
    p.setColor(foregroundRole(), QColor(cfg.window.foreground.c_str()));
    p.setColor(backgroundRole(), QColor(cfg.window.background.c_str()));
    setPalette(p);

    setLayout(&m_layout);
    setFont(QFont(cfg.font.name.c_str(), cfg.font.size));
    setFixedWidth(QApplication::primaryScreen()->size().width());
    setFixedHeight(fontMetrics().height() + fontMetrics().xHeight());

    m_input_line.setFixedWidth(fontMetrics().maxWidth() *
                               cfg.input_line_char_width);
    m_input_prefix.setFixedWidth(fontMetrics().maxWidth() *
                                 cfg.input_prefix_char_width);

    m_suggestions_counter.setFixedWidth(fontMetrics().maxWidth() * 10);

    m_layout.addWidget(&m_input_prefix);
    m_layout.addWidget(&m_input_line);
    m_layout.addWidget(&m_left_page_sign);
    m_layout.addWidget(&m_suggestion_line);
    m_layout.addWidget(&m_right_page_sign);
    m_layout.addWidget(&m_suggestions_counter);

    m_right_page_sign.setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    m_layout.setContentsMargins(0, 0, 10, 0);

    QObject::connect(&m_input_line, SIGNAL(textChanged(const QString&)), this,
                     SIGNAL(input_updated(const QString&)));
    QObject::connect(&m_suggestion_line,
                     SIGNAL(suggestion_selected(const QString&)), this,
                     SIGNAL(suggestion_selected(const QString&)));
    QObject::connect(
        &m_suggestion_line,
        SIGNAL(suggestion_page_requested(unsigned int, unsigned int)), this,
        SIGNAL(suggestion_page_requested(unsigned int, unsigned int)));
    QObject::connect(this, &CommandMenuWindow::suggestions_page_rejected,
                     &m_suggestion_line,
                     &SuggestionLine::suggestions_page_rejected);
    QObject::connect(&m_suggestion_line,
                     &SuggestionLine::current_suggestion_updated,
                     &m_input_prefix, &InputPrefix::update_prefix);
    QObject::connect(
        &m_suggestion_line, &SuggestionLine::current_position_updated,
        &m_suggestions_counter, &SuggestionCounter::current_position_updated);
  }

  void CommandMenuWindow::keyPressEvent(QKeyEvent* event) {

    auto user_action = key_event_to_action(event, m_key_mappings);

    switch (user_action) {
    case UserAction::EXIT_APPLICATION:
      QApplication::quit();
      break;

    case UserAction::SELECT_NEXT:
      m_suggestion_line.select_next();
      // update suggestions_counter
      break;

    case UserAction::SELECT_PREV:
      m_suggestion_line.select_prev();
      // update suggestions_counter
      break;

    case UserAction::ERASE_INPUT:
      m_input_line.clear();
      m_suggestion_line.clear();
      emit input_erased();
      // update suggestions_counter
      break;

    case UserAction::REMOVE_CHAR:
      m_input_line.remove_char();
      // update suggestions_counter
      break;

    case UserAction::SUBMIT:
      m_suggestion_line.submit_selection();
      m_input_line.clear();
      m_suggestion_line.clear();
      emit input_erased();
      break;

    case UserAction::PREV_PAGE:
      m_suggestion_line.request_prev_page();
      m_left_page_sign.set_active(true);
      return;

    case UserAction::NEXT_PAGE:
      m_suggestion_line.request_next_page();
      m_right_page_sign.set_active(true);
      return;

    case UserAction::NONE:
    default:
      break;
    }

    m_input_line.handle_key_press(event);
    QWidget::keyPressEvent(event);
  }

  void CommandMenuWindow::keyReleaseEvent(QKeyEvent* event) {
    auto user_action = key_event_to_action(event, m_key_mappings);

    switch (user_action) {
    case UserAction::PREV_PAGE:
      m_left_page_sign.set_active(false);
      return;

    case UserAction::NEXT_PAGE:
      m_right_page_sign.set_active(false);
      return;

    case UserAction::NONE:
    default:
      break;
    }

    QWidget::keyReleaseEvent(event);
  }

  void CommandMenuWindow::suggestion_page_prepared(
      const application::cmd_menu::Suggestions& suggestions) {
    m_suggestion_line.update_suggestions(suggestions);
  }

  void
  CommandMenuWindow::suggestions_count_updated(std::size_t suggestions_count) {
    m_suggestions_counter.set_count(suggestions_count);
  }

  CommandMenuWindow::~CommandMenuWindow() = default;

} // namespace application::ui
