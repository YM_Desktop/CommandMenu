#pragma once
#include <CustomLayout.h>
#include <InputLine.h>
#include <InputPrefix.h>
#include <KeyMappings.h>
#include <PageShiftDirectionSign.h>
#include <QWidget>
#include <SuggestionCounter.h>
#include <SuggestionLine.h>
#include <Suggestions.h>

namespace application::ui {
  struct Config;

  /**
   * @class CommandMenuWindow
   * @brief Represents Window
   */
  class CommandMenuWindow : public QWidget {
    Q_OBJECT

    /**
     * @brief Window layout.
     */
    CustomLayout m_layout;

    /**
     * @brief Input line.
     */
    InputPrefix m_input_prefix;

    /**
     * @brief Input line.
     */
    InputLine m_input_line;

    /**
     * @brief Suggestion line.
     */
    SuggestionLine m_suggestion_line;

    /**
     * @brief User defined actions and key mappings.
     */
    KeyMappings m_key_mappings;

    /**
     * @brief Left page shift sign.
     */
    PageShiftDirectionSign m_left_page_sign;

    /**
     * @brief Right page shift sign.
     */
    PageShiftDirectionSign m_right_page_sign;

    /**
     * @brief Suggestions counter.
     */
    SuggestionCounter m_suggestions_counter;

  public:
    /**
     * @fn CommandMenuWindow
     * @brief Constructor.
     * @param cfg: application::ui::Config
     */
    CommandMenuWindow(const Config& cfg);

    /**
     * @fn keyPressEvent
     * @brief Key press event handler.
     * @param event: QKeyEvent
     */
    void keyPressEvent(QKeyEvent* event) override;

    /**
     * @fn keyReleaseEvent
     * @brief Key release event handler.
     * @param event: QKeyEvent
     */
    void keyReleaseEvent(QKeyEvent* event) override;

    /**
     * @fn ~CommandMenuWindow
     * @brief Destructor.
     */
    ~CommandMenuWindow();

  public slots:
    /**
     * @fn suggestion_page_prepared
     * @brief handles suggestions page coming from CommandMenu.
     * @param suggestions: search suggestions
     */
    void suggestion_page_prepared(
        const application::cmd_menu::Suggestions& suggestions);

    /**
     * @fn suggestions_count_updated
     * @brief handles updated count of last suggestions.
     * @param suggestions_count
     */
    void suggestions_count_updated(std::size_t suggestions_count);

  signals:
    /**
     * @fn input_updated
     * @brief Emitted from input line to CommandMenu.
     */
    void input_updated(const QString&);

    /**
     * @fn input_erased
     * @brief Emitted from input line to CommandMenu.
     */
    void input_erased();

    /**
     * @fn suggestion_selected
     * @brief Emitted from suggestion line to CommandMenu.
     */
    void suggestion_selected(const QString&);

    /**
     * @fn suggestion_page_requested
     * @brief Emitted from suggestion line to CommandMenu.
     */
    void suggestion_page_requested(unsigned int, unsigned int);

    /**
     * @fn suggestions_page_rejected
     * @brief Emitted from suggestion line to CommandMenu.
     */
    void suggestions_page_rejected();
  };
} // namespace application::ui
