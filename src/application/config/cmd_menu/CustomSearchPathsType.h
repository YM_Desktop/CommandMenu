#pragma once
#include <filesystem>
#include <vector>

namespace application::cmd_menu {
  using CustomSearchPathsType = std::vector<std::filesystem::path>;
}
