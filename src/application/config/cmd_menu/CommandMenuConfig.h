#pragma once
#include "CustomSearchPathsType.h"

#include <ConfigKeys.h>
#include <array>

namespace application::cmd_menu {

  using namespace application::config::schema;

  /**
   * @struct CommandMenuConfig
   * @brief User-defined config of backend.
   */
  struct CommandMenuConfig {
    /**
     * @brief custom search paths (not taken from PATH).
     */
    CustomSearchPathsType custom_search_paths;

    /**
     * @brief boolean flag to use paths from PATH env var.
     */
    bool use_system_path{true};

    /**
     * @fn get
     * @brief returns member by ID
     * @tparam ID: member id
     * @return reference to member
     */
    template <ConfigSchemaKey ID>
    auto& get() noexcept {
      constexpr bool IS_CUSTOM_SEARCH_PATHS =
          ID == ConfigSchemaKey::CUSTOM_SEARCH_PATHS;
      if constexpr (IS_CUSTOM_SEARCH_PATHS)
        return custom_search_paths;

      constexpr bool IS_USE_SYSTEM_PATH =
          ID == ConfigSchemaKey::USE_SYSTEM_PATH;
      if constexpr (IS_USE_SYSTEM_PATH)
        return use_system_path;

      static_assert(IS_CUSTOM_SEARCH_PATHS || IS_USE_SYSTEM_PATH,
                    "Wrong ConfigSchemaKey provided in template!");
    }
  };
} // namespace application::cmd_menu
