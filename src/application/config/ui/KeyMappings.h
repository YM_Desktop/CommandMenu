#pragma once

#include "UserAction.h"

#include <qnamespace.h>
#include <set>

namespace application::ui {

  /**
   * @struct KeyMapping
   * @brief Represents user-defined key mapping.
   */
  struct KeyMapping {
    /**
     * @brief Code of key pressed by user.
     */
    Qt::Key key;

    /**
     * @brief Code of modifier, e.g. Ctrl, Alt, ... pressed by user.
     */
    Qt::KeyboardModifier modifier;

    /**
     * @brief Code of user action applied in window.
     */
    UserAction action;

    /**
     * @brief comparing operator
     * @param other: another KeyMapping to compare
     * @return true, if action is lesser than other, else false.
     */
    inline bool operator<(const KeyMapping& other) const noexcept {
      return action < other.action;
    }
  };

  using KeyMappings = std::set<KeyMapping>;
} // namespace application::ui
