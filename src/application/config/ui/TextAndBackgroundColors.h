#pragma once
#include <ConfigKeys.h>
#include <QColor>

namespace application::ui {

  using namespace application::config::schema;

  /**
   * @struct TextAndBackgroundColors
   * @brief Holds colors for Text and Background in Qt format.
   */
  struct TextAndBackgroundColors {

    /**
     * @brief foreground color in Qt format.
     */
    std::string foreground;

    /**
     * @brief background color in Qt format.
     */
    std::string background;

    /**
     * @fn get
     * @brief returns member by ID
     * @tparam ID: member id
     * @return reference to member
     */
    template <ConfigSchemaKey ID>
    auto& get() noexcept {

      constexpr bool IS_FOREGROUND = ID == ConfigSchemaKey::FOREGROUND;
      if constexpr (IS_FOREGROUND)
        return foreground;

      constexpr bool IS_BACKGROUND = ID == ConfigSchemaKey::BACKGROUND;
      if constexpr (IS_BACKGROUND)
        return background;

      static_assert(IS_FOREGROUND || IS_BACKGROUND,
                    "Wrong ConfigSchemaKey provided in template!");
    }
  };
} // namespace application::ui
