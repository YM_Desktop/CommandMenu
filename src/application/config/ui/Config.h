#pragma once
#include "KeyMappings.h"
#include "TextAndBackgroundColors.h"

#include <ConfigKeys.h>
#include <QFont>
#include <algorithm>
#include <set>

namespace application::ui {
  using namespace application::config::schema;

  /**
   * @struct application::ui::FontConfig
   * @brief Holds user defined config for font
   */
  struct FontConfig {

    /**
     * @brief font family name
     */
    std::string name{"Roboto Mono"};

    /**
     * @brief font size
     */
    int size{10};

    /**
     * @fn get
     * @brief returns member by ID
     * @tparam ID: member id
     * @return reference to member
     */
    template <ConfigSchemaKey ID>
    auto& get() noexcept {
      constexpr bool IS_NAME = ID == ConfigSchemaKey::NAME;
      if constexpr (IS_NAME)
        return name;

      constexpr bool IS_SIZE = ID == ConfigSchemaKey::SIZE;
      if constexpr (IS_SIZE)
        return size;

      static_assert(IS_NAME || IS_SIZE,
                    "Wrong ConfigSchemaKey provided in template!");
    }
  };

  /**
   * @struct application::ui::Config
   * @brief Holds user-specified config of UI.
   */
  struct Config {
    /**
     * @brief User input color palette.
     */
    TextAndBackgroundColors input_line{.foreground = "#acacac",
                                       .background = "#0c0c0c"};

    /**
     * @brief Suggestions color palette.
     */
    TextAndBackgroundColors suggestion_line{.foreground = "#acacac",
                                            .background = "#0c0c0c"};

    /**
     * @brief Selected suggestion color palette.
     */
    TextAndBackgroundColors suggestion_line_selected{.foreground = "#00f0f0",
                                                     .background = "#2c2c2c"};

    /**
     * @brief Page shift inactive colors.
     */
    TextAndBackgroundColors page_shift_inactive{.foreground = "#acacac",
                                                .background = "#0c0c0c"};

    /**
     * @brief Page shift active colors.
     */
    TextAndBackgroundColors page_shift_active{.foreground = "#020202",
                                              .background = "#ffffff"};

    /**
     * @brief Window color palette.
     */
    TextAndBackgroundColors window{.foreground = "#acacac",
                                   .background = "#0c0c0c"};

    /**
     * @brief Width of visible user input in chars.
     */
    unsigned int input_line_char_width{20u};

    /**
     * @brief Width of visible prefix for command.
     */
    unsigned int input_prefix_char_width{30u};

    /**
     * @brief Font specific data.
     */
    FontConfig font;

    /**
     * @brief User defined keyboard mappings.
     */
    KeyMappings key_mappings{
        { .key = Qt::Key_Escape,
         .modifier = Qt::NoModifier,
         .action = UserAction::EXIT_APPLICATION},
        {      .key = Qt::Key_N,
         .modifier = Qt::ControlModifier,
         .action = UserAction::SELECT_NEXT     },
        {      .key = Qt::Key_P,
         .modifier = Qt::ControlModifier,
         .action = UserAction::SELECT_PREV     },
        {      .key = Qt::Key_W,
         .modifier = Qt::ControlModifier,
         .action = UserAction::ERASE_INPUT     },
        {      .key = Qt::Key_H,
         .modifier = Qt::ControlModifier,
         .action = UserAction::REMOVE_CHAR     },
        {      .key = Qt::Key_M,
         .modifier = Qt::ControlModifier,
         .action = UserAction::SUBMIT          },
        {   .key = Qt::Key_Less,
         .modifier = Qt::ShiftModifier,
         .action = UserAction::PREV_PAGE       },
        {.key = Qt::Key_Greater,
         .modifier = Qt::ShiftModifier,
         .action = UserAction::NEXT_PAGE       },
    };

    /**
     * @fn get
     * @brief returns member by ID
     * @tparam ID: member id
     * @return reference to member
     */
    template <ConfigSchemaKey ID>
    auto& get() noexcept {

      constexpr bool IS_INPUT_LINE = ID == ConfigSchemaKey::INPUT_LINE;
      if constexpr (IS_INPUT_LINE)
        return input_line;

      constexpr bool IS_SUGGESTION_LINE =
          ID == ConfigSchemaKey::SUGGESTION_LINE;
      if constexpr (IS_SUGGESTION_LINE)
        return suggestion_line;

      constexpr bool IS_SUGGESTION_LINE_SELECTED =
          ID == ConfigSchemaKey::SUGGESTION_LINE_SELECTED;
      if constexpr (IS_SUGGESTION_LINE_SELECTED)
        return suggestion_line_selected;

      constexpr bool IS_PAGE_SHIFT_ACTIVE =
          ID == ConfigSchemaKey::PAGE_SHIFT_ACTIVE;
      if constexpr (IS_PAGE_SHIFT_ACTIVE)
        return page_shift_active;

      constexpr bool IS_PAGE_SHIFT_INACTIVE =
          ID == ConfigSchemaKey::PAGE_SHIFT_INACTIVE;
      if constexpr (IS_PAGE_SHIFT_INACTIVE)
        return page_shift_inactive;

      constexpr bool IS_WINDOW = ID == ConfigSchemaKey::WINDOW;
      if constexpr (IS_WINDOW)
        return window;

      constexpr bool IS_INPUT_MAX_WIDTH =
          ID == ConfigSchemaKey::INPUT_MAX_WIDTH;
      if constexpr (IS_INPUT_MAX_WIDTH)
        return input_line_char_width;

      constexpr bool IS_PREFIX_MAX_WIDTH =
          ID == ConfigSchemaKey::PREFIX_MAX_WIDTH;
      if constexpr (IS_PREFIX_MAX_WIDTH)
        return input_prefix_char_width;

      constexpr bool IS_FONT = ID == ConfigSchemaKey::FONT;
      if constexpr (IS_FONT)
        return font;

      static_assert(IS_INPUT_LINE || IS_SUGGESTION_LINE ||
                        IS_SUGGESTION_LINE_SELECTED || IS_PAGE_SHIFT_ACTIVE ||
                        IS_PAGE_SHIFT_INACTIVE || IS_WINDOW ||
                        IS_INPUT_MAX_WIDTH || IS_PREFIX_MAX_WIDTH || IS_FONT,
                    "Wrong ConfigSchemaKey provided in template!");
    }
  };
} // namespace application::ui
