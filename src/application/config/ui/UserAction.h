#pragma once

namespace application::ui {
  /**
   * @class UserAction
   * @brief Enum of user actions
   */
  enum class UserAction : unsigned char {
    NONE,
    EXIT_APPLICATION,
    SELECT_NEXT,
    SELECT_PREV,
    ERASE_INPUT,
    REMOVE_CHAR,
    SUBMIT,
    NEXT_PAGE,
    PREV_PAGE
  };
} // namespace application::ui
