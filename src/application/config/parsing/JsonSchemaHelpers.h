
#pragma once

#include <CustomSearchPathsType.h>
#include <QJsonArray>
#include <QJsonObject>
#include <SchemaHelpers.h>
#include <type_traits>

namespace application::config::schema {

  /**
   * @copydoc Helpers
   */
  template <>
  struct Helpers<QJsonValue> {

    /**
     * @fn node_exists
     * @brief Checks whether node exists
     * @param node
     * @return true if exists, false otherwise
     */
    static bool node_exists(const QJsonValue& node) noexcept {

      if (node.isObject())
        return !node.toObject().isEmpty();

      if (QJsonValue::Null == node.type())
        return false;

      return true;
    }

    /**
     * @fn get_node_value
     * @tparam NodeValueType: expected type of value
     * @param node
     * @return NodeValueType
     */
    template <typename NodeValueType>
    static NodeValueType get_node_value(const QJsonValue& node) {
      constexpr bool IS_STRING = std::is_same_v<NodeValueType, std::string>;
      if constexpr (IS_STRING) {
        if (!node.isString())
          throw std::logic_error("Config item type mismatch!");
        return node.toString().toStdString();
      }

      constexpr bool IS_INTEGER = std::is_same_v<NodeValueType, int> ||
                                  std::is_same_v<NodeValueType, unsigned int>;
      if constexpr (IS_INTEGER) {
        if (!node.isDouble())
          throw std::logic_error("Config item type mismatch!");
        return node.toInt();
      }

      constexpr bool IS_BOOL = std::is_same_v<NodeValueType, bool>;
      if constexpr (IS_BOOL) {
        if (!node.isBool())
          throw std::logic_error("Config item type mismatch!");
        return node.toBool();
      }

      constexpr bool IS_CUSTOM_SEARCH_PATHS =
          std::is_same_v<NodeValueType,
                         application::cmd_menu::CustomSearchPathsType>;
      if constexpr (IS_CUSTOM_SEARCH_PATHS) {
        if (!node.isArray())
          throw std::logic_error("Config item type mismatch!");

        QJsonArray arr = node.toArray();

        application::cmd_menu::CustomSearchPathsType paths;
        paths.reserve(arr.size());

        for (std::size_t i{0ul}; i < arr.size(); ++i) {
          paths.push_back(arr[i].toString().toStdString());
        }

        return paths;
      }

      static_assert(IS_BOOL || IS_INTEGER || IS_STRING ||
                        IS_CUSTOM_SEARCH_PATHS,
                    "Unknown value type provided!");
    }

    /**
     * @fn node_under_key
     * @brief Gets node object under specified key
     * @param parent: node where we take subnode
     * @param config_key: key of subnode
     * @return Node
     */
    static QJsonValue node_under_key(const QJsonValue& parent,
                                     ConfigSchemaKey config_key) {
      auto json_object = parent.toObject();
      if (!json_object.contains(ConfigSchemaKeys.at(config_key).c_str())) {
        return QJsonValue();
      }

      return json_object[ConfigSchemaKeys.at(config_key).c_str()];
    }
  };

} // namespace application::config::schema
