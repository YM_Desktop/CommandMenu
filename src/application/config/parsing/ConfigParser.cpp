#include "ConfigParser.h"

#include "JsonParser.h"
#include "YamlParser.h"

#include <ApplicationConfig.h>
#include <UserAction.h>

namespace {

  std::unique_ptr<application::config::parsing::ConfigParser>
  get_parser_by_extension(std::string&& ext) {

    static constexpr char json_config_extension[]{".json"};
    static constexpr char yaml_config_extension[]{".yaml"};

    if (ext == json_config_extension) {
      return std::make_unique<application::config::parsing::JsonParser>();
    }

    if (ext == yaml_config_extension) {
      return std::make_unique<application::config::parsing::YamlParser>();
    }

    throw std::runtime_error("Unknown config format!");
  }

} // namespace

namespace application::config::parsing {

  std::unique_ptr<application::ApplicationConfig>
  ConfigParser::parse(const std::filesystem::path& filepath) {

    if (!std::filesystem::exists(filepath)) {
      return std::make_unique<application::ApplicationConfig>();
    }

    auto parser = get_parser_by_extension(filepath.extension());
    auto config = parser->parse_file(filepath);
    return config;
  }
} // namespace application::config::parsing
