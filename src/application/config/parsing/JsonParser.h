#pragma once
#include "ConfigParser.h"

#include <filesystem>
#include <memory>

class QJsonDocument;

namespace application {
  struct ApplicationConfig;
}

namespace application::config::parsing {

  /**
   * @class JsonParser
   * @brief handles parsing of JSON config
   */
  class JsonParser : public ConfigParser {

    /**
     * @fn read_file
     * @param filepath
     * @return Json parsed doc
     */
    QJsonDocument read_file(const std::filesystem::path& filepath) const;

    /**
     * @fn parse_config
     * @param json: json doc
     * @return parsed application config
     */
    std::unique_ptr<application::ApplicationConfig>
    parse_config(const QJsonDocument& json) const;

  public:
    /**
     * @fn JsonParser
     * @brief Constructor.
     */
    JsonParser();

    /**
     * @fn ~JsonParser
     * @brief Destructor.
     */
    ~JsonParser();

    /**
     * @fn parse_file
     * @param filepath
     * @return parsed application config
     */
    std::unique_ptr<application::ApplicationConfig>
    parse_file(const std::filesystem::path& filepath) const override;
  };
} // namespace application::config::parsing
