#pragma once

#include <CustomSearchPathsType.h>
#include <SchemaHelpers.h>
#include <stdexcept>
#include <yaml-cpp/yaml.h>

namespace application::config::schema {

  /**
   * @copydoc Helpers
   */
  template <>
  struct Helpers<YAML::Node> {

    /**
     * @fn node_exists
     * @brief Checks whether node exists
     * @param node
     * @return true if exists, false otherwise
     */
    static bool node_exists(const YAML::Node& node) noexcept {
      return static_cast<bool>(node);
    }

    /**
     * @fn get_node_value
     * @tparam NodeValueType: expected type of value
     * @param node
     * @return NodeValueType
     */
    template <typename NodeValueType>
    static NodeValueType get_node_value(const YAML::Node& node) {

      constexpr bool IS_CUSTOM_SEARCH_PATHS =
          std::is_same_v<NodeValueType,
                         application::cmd_menu::CustomSearchPathsType>;

      if constexpr (IS_CUSTOM_SEARCH_PATHS) {

        const auto& arr = node;

        application::cmd_menu::CustomSearchPathsType paths;
        paths.reserve(arr.size());

        for (std::size_t i{0ul}; i < arr.size(); ++i) {
          paths.push_back(arr[i].as<std::string>());
        }

        return paths;
      }

      else
        return node.as<NodeValueType>();
    }

    /**
     * @fn node_under_key
     * @brief Gets node object under specified key
     * @param parent: node where we take subnode
     * @param config_key: key of subnode
     * @return Node
     */
    static YAML::Node node_under_key(const YAML::Node& parent,
                                     ConfigSchemaKey config_key) {
      return parent[ConfigSchemaKeys.at(config_key)];
    }
  };

} // namespace application::config::schema
