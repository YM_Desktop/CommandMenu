#include "JsonParser.h"

#include "JsonSchemaHelpers.h"

#include <ApplicationConfig.h>
#include <ApplicationConfigSchema.h>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <UserAction.h>
#include <functional>

namespace application::config::parsing {

  QJsonDocument
  JsonParser::read_file(const std::filesystem::path& filepath) const {
    QFile file(filepath.c_str());
    if (!file.open(QIODeviceBase::ReadOnly))
      throw std::runtime_error("Config file is not readable!");

    QByteArray bytes = file.readAll();

    QJsonParseError parse_error;
    QJsonDocument doc = QJsonDocument::fromJson(bytes, &parse_error);

    if (parse_error.error != QJsonParseError::NoError)
      throw std::logic_error(parse_error.errorString().toStdString());

    return doc;
  }

  std::unique_ptr<application::ApplicationConfig>
  JsonParser::parse_config(const QJsonDocument& json) const {

    std::unique_ptr<application::ApplicationConfig> cfg(new ApplicationConfig);
    const QJsonValue object = json.object();

    application::config::schema::APPLICATION_CONFIG::validate(object, *cfg);

    return cfg;
  }

  std::unique_ptr<application::ApplicationConfig>
  JsonParser::parse_file(const std::filesystem::path& filepath) const {
    auto json_doc = read_file(filepath);
    auto config = parse_config(json_doc);
    return config;
  }

  JsonParser::JsonParser() = default;
  JsonParser::~JsonParser() = default;
} // namespace application::config::parsing
