#pragma once
#include "ConfigParser.h"

#include <filesystem>
#include <memory>

namespace application {
  struct ApplicationConfig;
}

namespace application::config::parsing {

  /**
   * @class YamlParser
   * @brief handles parsing of JSON config
   */
  class YamlParser : public ConfigParser {

  public:
    /**
     * @fn YamlParser
     * @brief Constructor.
     */
    YamlParser();

    /**
     * @fn ~YamlParser
     * @brief Destructor.
     */
    ~YamlParser();

    /**
     * @fn parse_file
     * @param filepath
     * @return parsed application config
     */
    std::unique_ptr<application::ApplicationConfig>
    parse_file(const std::filesystem::path& filepath) const override;
  };
} // namespace application::config::parsing
