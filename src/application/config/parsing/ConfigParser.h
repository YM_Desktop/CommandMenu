#pragma once
#include <filesystem>
#include <memory>

namespace application {
  struct ApplicationConfig;
}

namespace application::config::parsing {

  /**
   * @class ConfigParser
   * @brief Abstraction for config parsers
   */
  class ConfigParser {

  public:
    /**
     * @fn parse_file
     * @param filepath: path to config file.
     * @return parsed ApplicationConfig
     */
    virtual std::unique_ptr<application::ApplicationConfig>
    parse_file(const std::filesystem::path& filepath) const = 0;

    /**
     * @brief Destructor.
     */
    virtual ~ConfigParser() = default;

    /**
     * @fn parse
     * @param filepath
     * @return ApplicationConfig
     */
    static std::unique_ptr<application::ApplicationConfig>
    parse(const std::filesystem::path& filepath);
  };
} // namespace application::config::parsing
