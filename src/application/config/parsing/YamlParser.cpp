#include "YamlParser.h"

#include "YamlSchemaHelpers.h"

#include <ApplicationConfig.h>
#include <ApplicationConfigSchema.h>
#include <yaml-cpp/yaml.h>

namespace {

  std::unique_ptr<application::ApplicationConfig>
  parse_config(const YAML::Node& yaml) {
    auto cfg = std::make_unique<application::ApplicationConfig>();
    application::config::schema::APPLICATION_CONFIG::validate(yaml, *cfg);
    return cfg;
  }

} // namespace

namespace application::config::parsing {

  YamlParser::YamlParser() = default;
  YamlParser::~YamlParser() = default;

  std::unique_ptr<application::ApplicationConfig>
  YamlParser::parse_file(const std::filesystem::path& filepath) const {
    auto parsed_yaml = YAML::LoadFile(filepath);
    auto config = parse_config(parsed_yaml);
    return config;
  }
} // namespace application::config::parsing
