#pragma once

#include <CommandMenuConfig.h>
#include <Config.h>
#include <ConfigKeys.h>
#include <algorithm>

namespace application {
  using namespace application::config::schema;

  /**
   * @struct application::ApplicationConfig
   * Holds user-defined application configuration.
   */
  struct ApplicationConfig {

    /**
     * @brief UI configuration.
     */
    application::ui::Config ui_cfg;

    /**
     * @brief Backend configuration.
     */
    application::cmd_menu::CommandMenuConfig menu_cfg;

    /**
     * @fn get
     * @brief returns member by ID
     * @tparam ID: member id
     * @return reference to member
     */
    template <ConfigSchemaKey ID>
    auto& get() noexcept {

      constexpr bool IS_UI = ID == ConfigSchemaKey::UI;
      if constexpr (IS_UI)
        return ui_cfg;

      constexpr bool IS_MENU = ID == ConfigSchemaKey::MENU;
      if constexpr (IS_MENU)
        return menu_cfg;

      static_assert(IS_UI || IS_MENU,
                    "Wrong ConfigSchemaKey provided in template!");
    }
  };
} // namespace application
