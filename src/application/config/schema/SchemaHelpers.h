#pragma once
#include "ConfigKeys.h"

namespace application::config::schema {

  /**
   * @class Helpers
   * Provides methods for nodes implementation of which
   * is overriden in compile time.
   * @tparam NodeType: type of config node
   */
  template <typename NodeType>
  struct Helpers {
    /**
     * @fn node_exists
     * @brief Checks whether node exists
     * @param node
     * @return true if exists, false otherwise
     */
    static bool node_exists(const NodeType& node) noexcept { return false; }

    /**
     * @fn get_node_value
     * @tparam NodeValueType: expected type of value
     * @param node
     * @return NodeValueType
     */
    template <typename NodeValueType>
    static NodeValueType get_node_value(const NodeType& node) {
      return NodeValueType();
    }

    /**
     * @fn node_under_key
     * @brief Gets node object under specified key
     * @param parent: node where we take subnode
     * @param config_key: key of subnode
     * @return Node
     */
    static const NodeType& node_under_key(const NodeType& parent,
                                          ConfigSchemaKey config_key) {
      return parent;
    }
  };

} // namespace application::config::schema
