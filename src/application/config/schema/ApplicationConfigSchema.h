#pragma once
#include "CommandMenuConfig.h"
#include "ObjectSchemaKey.h"

#include <string>
#include <vector>

namespace application::config::schema {

  using FOREGROUND =
      SchemaKey<ConfigSchemaKey::FOREGROUND, std::string, KeyType::OPTIONAL>;
  using BACKGROUND =
      SchemaKey<ConfigSchemaKey::BACKGROUND, std::string, KeyType::REQUIRED>;

  using INPUT_LINE = ObjectSchemaKey<ConfigSchemaKey::INPUT_LINE,
                                     KeyType::OPTIONAL, FOREGROUND, BACKGROUND>;
  using SUGGESTION_LINE =
      ObjectSchemaKey<ConfigSchemaKey::SUGGESTION_LINE, KeyType::OPTIONAL,
                      FOREGROUND, BACKGROUND>;
  using SUGGESTION_LINE_SELECTED =
      ObjectSchemaKey<ConfigSchemaKey::SUGGESTION_LINE_SELECTED,
                      KeyType::OPTIONAL, FOREGROUND, BACKGROUND>;
  using PAGE_SHIFT_ACTIVE =
      ObjectSchemaKey<ConfigSchemaKey::PAGE_SHIFT_ACTIVE, KeyType::OPTIONAL,
                      FOREGROUND, BACKGROUND>;
  using PAGE_SHIFT_INACTIVE =
      ObjectSchemaKey<ConfigSchemaKey::PAGE_SHIFT_INACTIVE, KeyType::OPTIONAL,
                      FOREGROUND, BACKGROUND>;
  using WINDOW = ObjectSchemaKey<ConfigSchemaKey::WINDOW, KeyType::OPTIONAL,
                                 FOREGROUND, BACKGROUND>;

  using INPUT_MAX_WIDTH = SchemaKey<ConfigSchemaKey::INPUT_MAX_WIDTH,
                                    unsigned int, KeyType::REQUIRED>;
  using PREFIX_MAX_WIDTH = SchemaKey<ConfigSchemaKey::PREFIX_MAX_WIDTH,
                                     unsigned int, KeyType::OPTIONAL>;

  using NAME = SchemaKey<ConfigSchemaKey::NAME, std::string, KeyType::REQUIRED>;
  using SIZE = SchemaKey<ConfigSchemaKey::SIZE, int, KeyType::OPTIONAL>;
  using FONT =
      ObjectSchemaKey<ConfigSchemaKey::FONT, KeyType::OPTIONAL, NAME, SIZE>;

  using UI = ObjectSchemaKey<ConfigSchemaKey::UI, KeyType::OPTIONAL, INPUT_LINE,
                             SUGGESTION_LINE, SUGGESTION_LINE_SELECTED,
                             PAGE_SHIFT_ACTIVE, PAGE_SHIFT_INACTIVE, WINDOW,
                             INPUT_MAX_WIDTH, PREFIX_MAX_WIDTH, FONT>;

  using CUSTOM_SEARCH_PATHS =
      SchemaKey<ConfigSchemaKey::CUSTOM_SEARCH_PATHS,
                application::cmd_menu::CustomSearchPathsType,
                KeyType::OPTIONAL>;
  using USE_SYSTEM_PATH =
      SchemaKey<ConfigSchemaKey::USE_SYSTEM_PATH, bool, KeyType::REQUIRED>;

  using MENU = ObjectSchemaKey<ConfigSchemaKey::MENU, KeyType::REQUIRED,
                               CUSTOM_SEARCH_PATHS, USE_SYSTEM_PATH>;

  using APPLICATION_CONFIG =
      ObjectSchemaKey<ConfigSchemaKey::APP_CONFIG, KeyType::REQUIRED, UI, MENU>;

} // namespace application::config::schema
