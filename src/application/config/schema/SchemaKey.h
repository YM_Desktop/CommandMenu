#pragma once
#include "ApplicationConfig.h"
#include "ConfigKeys.h"
#include "KeyType.h"
#include "SchemaHelpers.h"

#include <functional>
#include <stdexcept>
#include <string>

namespace application::config::schema {

  /**
   * @class SchemaKey
   * @brief Represents schema for parsing config key.
   * @tparam Key: ConfigSchemaKey for instance.
   * @tparam VType: ValueType expected under key.
   * @tparam Type: REQUIRED or OPTIONAL
   */
  template <ConfigSchemaKey Key, typename VType, KeyType Type>
  class SchemaKey {
    static_assert(!std::is_same_v<VType, void>,
                  "Cannot instantiate schema for void key\n");

  public:
    /**
     * @brief Constructor.
     */
    SchemaKey<Key, VType, Type>() = default;

    /**
     * @brief Destructor.
     */
    ~SchemaKey<Key, VType, Type>() = default;

    /**
     * @fn key
     * @return ConfigSchemaKey
     */
    static constexpr inline ConfigSchemaKey key() noexcept { return Key; }

    /**
     * @fn optional
     * @return true if Type is OPTIONAL, false otherwise.
     */
    static constexpr inline bool optional() noexcept {
      return KeyType::OPTIONAL == Type;
    }

    /**
     * @fn required
     * @return true if Type is REQUIRED, false otherwise.
     */
    static constexpr inline bool required() noexcept {
      return KeyType::REQUIRED == Type;
    }

    /**
     * @fn is_value_type
     * @return true if T is same as value type, false otherwise.
     */
    template <typename T>
    static constexpr inline bool is_value_type() noexcept {
      return std::is_same_v<T, VType>;
    }

    /**
     * @fn validate
     * @brief validates given config node by current schema
     * @tparam ConfigNode: type of config, e.g. YAML::Node
     */
    template <typename ConfigNode, typename ConfigItem>
    static void validate(const ConfigNode& node, ConfigItem& cfg) {

      if constexpr (required()) {
        if (!Helpers<ConfigNode>::node_exists(node)) {
          const std::string& key_string =
              application::config::schema::ConfigSchemaKeys.at(key());
          throw std::logic_error(std::string("Required key ") + key_string +
                                 std::string(" not provided!"));
        }
      }

      if constexpr (optional()) {
        if (!Helpers<ConfigNode>::node_exists(node)) {
          return;
        }
      }

      // Fill in application config
      cfg = Helpers<ConfigNode>::template get_node_value<VType>(node);
    }
  };
} // namespace application::config::schema
