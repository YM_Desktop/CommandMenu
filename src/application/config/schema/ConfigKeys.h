#pragma once

#include <string>
#include <unordered_map>

namespace application::config::schema {

  enum class ConfigSchemaKey {
    NONE,

    UI,
    INPUT_LINE,
    SUGGESTION_LINE,
    SUGGESTION_LINE_SELECTED,
    PAGE_SHIFT_ACTIVE,
    PAGE_SHIFT_INACTIVE,
    WINDOW,

    FOREGROUND,
    BACKGROUND,

    INPUT_MAX_WIDTH,
    PREFIX_MAX_WIDTH,

    FONT,
    NAME,
    SIZE,

    MENU,
    CUSTOM_SEARCH_PATHS,
    USE_SYSTEM_PATH,

    APP_CONFIG
  };

  const std::unordered_map<ConfigSchemaKey, std::string> ConfigSchemaKeys{
      {                      ConfigSchemaKey::UI,                       "ui"},
      {              ConfigSchemaKey::INPUT_LINE,               "input_line"},
      {         ConfigSchemaKey::SUGGESTION_LINE,          "suggestion_line"},
      {ConfigSchemaKey::SUGGESTION_LINE_SELECTED, "suggestion_line_selected"},
      {       ConfigSchemaKey::PAGE_SHIFT_ACTIVE,        "page_shift_active"},
      {     ConfigSchemaKey::PAGE_SHIFT_INACTIVE,      "page_shift_inactive"},
      {                  ConfigSchemaKey::WINDOW,                   "window"},

      {              ConfigSchemaKey::FOREGROUND,               "foreground"},
      {              ConfigSchemaKey::BACKGROUND,               "background"},
      {         ConfigSchemaKey::INPUT_MAX_WIDTH,          "input_max_width"},
      {        ConfigSchemaKey::PREFIX_MAX_WIDTH,         "prefix_max_width"},
      {                    ConfigSchemaKey::FONT,                     "font"},
      {                    ConfigSchemaKey::NAME,                     "name"},
      {                    ConfigSchemaKey::SIZE,                     "size"},

      {                    ConfigSchemaKey::MENU,                     "menu"},
      {     ConfigSchemaKey::CUSTOM_SEARCH_PATHS,      "custom_search_paths"},
      {         ConfigSchemaKey::USE_SYSTEM_PATH,          "use_system_path"}
  };
} // namespace application::config::schema
