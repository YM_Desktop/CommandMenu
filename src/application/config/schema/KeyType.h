#pragma once

namespace application::config::schema {

  enum class KeyType { OPTIONAL, REQUIRED };

}
