#pragma once
#include "ApplicationConfig.h"
#include "ConfigKeys.h"
#include "KeyType.h"
#include "SchemaHelpers.h"
#include "SchemaKey.h"

#include <algorithm>
#include <functional>
#include <tuple>

namespace application::config::schema {

  /**
   * @class ObjectSchemaKey
   * Describes config object with subkeys.
   * @tparam Key: id of schema key
   * @tparam Type: REQUIRED or OPTIONAL
   * @tparam Subkeys: list of subkeys
   */
  template <ConfigSchemaKey Key, KeyType Type, typename... Subkeys>
  class ObjectSchemaKey {

    /**
     * @fn validate_other_keys
     * @brief validates list of keys
     * @tparam ConfigNode: type of config, e.g. YAML::Node
     * @tparam CurrentKey: current config key to validate
     * @tparam OtherKeys: list of other keys
     */
    template <typename ConfigNode, class ConfigItem, typename CurrentKey,
              typename... OtherKeys>
    static void validate_other_keys(const ConfigNode& node, ConfigItem& cfg) {

      const auto& current_node =
          Helpers<ConfigNode>::node_under_key(node, CurrentKey::key());

      constexpr ConfigSchemaKey current_key = CurrentKey::key();
      CurrentKey::validate(current_node, cfg.template get<current_key>());

      if constexpr (sizeof...(OtherKeys) > 0)
        validate_other_keys<ConfigNode, ConfigItem, OtherKeys...>(node, cfg);
    }

    /**
     * @fn all_subkeys_optional
     * @return true if all subkeys are optional else false.
     */
    template <typename T, typename... OtherSubkeys>
    static constexpr inline bool all_subkeys_optional() noexcept {

      if constexpr (T::required())
        return false;

      else if constexpr (sizeof...(OtherSubkeys) == 0)
        return true;

      else
        return all_subkeys_optional<OtherSubkeys...>();
    }

  public:
    /**
     * @brief Constructor.
     */
    ObjectSchemaKey<Key, Type, Subkeys...>() noexcept {

      static_assert(sizeof...(Subkeys) != 0,
                    "No arguments provided for ObjectSchema!\n");

      constexpr bool subkeys_are_optional = all_subkeys_optional<Subkeys...>();
      static_assert(!subkeys_are_optional,
                    "Object must have at least 1 REQUIRED key!\n");
    }

    /**
     * @brief Destructor.
     */
    ~ObjectSchemaKey<Key, Type, Subkeys...>() = default;

    /**
     * @fn optional
     * @return true if type is OPTIONAL, false otherwise.
     */
    static constexpr inline bool optional() noexcept {
      return KeyType::OPTIONAL == Type;
    }

    /**
     * @fn required
     * @return true if type is REQUIRED, false otherwise.
     */
    static constexpr inline bool required() noexcept {
      return KeyType::REQUIRED == Type;
    }

    /**
     * @fn key
     * @return ConfigSchemaKey id of current instance.
     */
    static constexpr inline ConfigSchemaKey key() noexcept { return Key; }

    /**
     * @fn subkeys_count
     * @return number of subkeys in current instance.
     */
    constexpr inline std::size_t subkeys_count() const noexcept {
      return sizeof...(Subkeys);
    }

    /**
     * @fn validate
     * @brief validates given config node by current schema
     * @tparam ConfigNode: type of config, e.g. YAML::Node
     * @param node: config node instance
     * @param cfg: ApplicationConfig
     */
    template <typename ConfigNode, class ConfigItem>
    static void validate(const ConfigNode& node, ConfigItem& cfg) {

      if constexpr (optional()) {
        if (!Helpers<ConfigNode>::node_exists(node)) {
          return;
        }
      }

      if constexpr (required()) {
        if (!Helpers<ConfigNode>::node_exists(node)) {
          const std::string& key_string =
              application::config::schema::ConfigSchemaKeys.at(key());
          throw std::logic_error(std::string("Required key ") + key_string +
                                 std::string(" not provided!"));
        }
      }

      validate_other_keys<ConfigNode, ConfigItem, Subkeys...>(node, cfg);
    }
  };
} // namespace application::config::schema
