#include "SearchEngine.h"

#include "CommandsDB.h"

#include <string_view>

namespace application::cmd_menu {
  SearchEngine::SearchEngine(const CommandMenuConfig& cfg)
      : m_commands_db(std::make_unique<CommandsDB>(cfg)) {
    m_pushed_chars.reserve(64);
  }

  SearchEngine::~SearchEngine() = default;

  std::optional<std::reference_wrapper<const Suggestions>>
  SearchEngine::push(char c) noexcept {
    m_pushed_chars.push_back(c);
    auto& last_suggestions = m_suggestions.emplace_back();
    const auto& binaries = m_commands_db->binaries();
    for (const auto& [bin_dir, bins] : binaries) {
      std::for_each(bins.cbegin(), bins.cend(),
                    [&pushed_chars = m_pushed_chars, &last_suggestions,
                     &dir = bin_dir](const std::string& bin_name) {
                      if (bin_name.starts_with(pushed_chars)) {
                        last_suggestions.emplace(bin_name, dir);
                      }
                    });
    }

    if (last_suggestions.empty()) {
      return std::nullopt;
    }

    return m_suggestions.back();
  }

  std::optional<std::reference_wrapper<const Suggestions>>
  SearchEngine::pop() noexcept {
    if (!m_pushed_chars.empty())
      m_pushed_chars.pop_back();

    if (m_suggestions.empty()) {
      return std::nullopt;
    }

    m_suggestions.erase(std::prev(m_suggestions.end()));
    if (m_suggestions.empty() || m_suggestions.back().empty()) {
      return std::nullopt;
    }

    return m_suggestions.back();
  }

  std::optional<std::reference_wrapper<const Suggestions>>
  SearchEngine::suggestions() const noexcept {
    if (m_suggestions.empty())
      return std::nullopt;

    if (m_suggestions.back().empty())
      return std::nullopt;

    return m_suggestions.back();
  }

  void SearchEngine::clear() noexcept {
    m_suggestions.clear();
    m_pushed_chars.clear();
  }
} // namespace application::cmd_menu
