#pragma once
#include <Suggestions.h>
#include <functional>
#include <list>
#include <memory>
#include <optional>

namespace application::cmd_menu {
  class CommandsDB;
  struct CommandMenuConfig;
} // namespace application::cmd_menu

namespace application::cmd_menu {
  /**
   * @class SearchEngine
   * @brief Represents command search logic.
   */
  class SearchEngine {

    /**
     * @brief Commands database instance.
     */
    std::unique_ptr<CommandsDB> m_commands_db;

    /**
     * @brief List of current suggestions.
     */
    std::list<Suggestions> m_suggestions;

    /**
     * @brief User-inputed chars.
     */
    std::string m_pushed_chars;

  public:
    /**
     * @fn SearchEngine
     * @brief Constructor
     * @param cfg: CommandMenuConfig.
     */
    SearchEngine(const CommandMenuConfig& cfg);

    /**
     * @fn ~SearchEngine
     * @brief Destructor.
     */
    ~SearchEngine();

    /**
     * @fn push
     * @brief Adds char to pushed chars and updates suggestions.
     * @param c : user input char.
     * @return new suggestions if there are some, std::nullopt otherwise.
     */
    std::optional<std::reference_wrapper<const Suggestions>>
    push(char c) noexcept;

    /**
     * @fn pop
     * Removes char from pushed chars and removes last suggestions
     * associated with chars.
     * @return last suggestions if there are some, std::nullopt otherwise.
     */
    std::optional<std::reference_wrapper<const Suggestions>> pop() noexcept;

    /**
     * @fn suggestions
     * @return last suggestions if there are some, std::nullopt otherwise.
     */
    std::optional<std::reference_wrapper<const Suggestions>>
    suggestions() const noexcept;

    /**
     * @fn clear
     * @brief clears pushed chars and suggestions list.
     */
    void clear() noexcept;
  };
} // namespace application::cmd_menu
