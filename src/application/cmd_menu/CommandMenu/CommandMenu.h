#pragma once

#include <QObject>
#include <QString>
#include <SearchEngine.h>
#include <Suggestions.h>
#include <string_view>
#include <vector>

Q_DECLARE_METATYPE(application::cmd_menu::Suggestions);

namespace application::cmd_menu {
  struct CommandMenuConfig;
}

namespace application::cmd_menu {

  /**
   * @class CommandMenu
   * Holds logic of searching and providing search results.
   */
  class CommandMenu : public QObject {
    Q_OBJECT

    /**
     * @brief holds input size since last request.
     */
    std::size_t m_prev_input_size;

    /**
     * @brief holds search engine instance.
     */
    SearchEngine m_search_engine;

  public:
    /**
     * @fn CommandMenu
     * @brief Constructor
     * @param cfg : CommandMenuConfig.
     */
    CommandMenu(const CommandMenuConfig& cfg);

    /**
     * @fn ~CommandMenu
     * @brief Destructor
     */
    ~CommandMenu();

  public slots:
    /**
     * @fn application::cmd_menu::CommandMenu::process_input_update
     * @brief defines whether user added/removed char to/from input.
     * @param input: updated user input.
     */
    void process_input_update(const QString& input) noexcept;

    /**
     * @fn application::cmd_menu::CommandMenu::suggestion_page_requested
     * @brief process pagination request for search result.
     * @param start: start index from latest result search.
     * @param count: how much elements to retrieve.
     */
    void suggestion_page_requested(unsigned int start,
                                   unsigned int count) noexcept;

    /**
     * @fn application::cmd_menu::CommandMenu::apply_suggestion
     * @brief process suggestion selected by user.
     * @param selected_suggestion: selected suggestion.
     */
    void apply_suggestion(const QString& selected_suggestion) noexcept;

    /**
     * @fn application::cmd_menu::CommandMenu::process_input_erase
     * @brief erases current search if user reset input.
     */
    void process_input_erase() noexcept;

  signals:
    /**
     * @fn application::cmd_menu::CommandMenu::suggestion_page_prepared
     * @brief signal. Emitted if pagination request validly processed.
     */
    void suggestion_page_prepared(const Suggestions& suggestions);

    /**
     * @fn application::cmd_menu::CommandMenu::suggestions_page_rejected
     * @brief signal. Emitted if pagination request rejected.
     */
    void suggestions_page_rejected();

    /**
     * @fn application::cmd_menu::CommandMenu::suggestions_count_updated
     * @brief signal. Emitted if suggestions updated by input changes.
     */
    void suggestions_count_updated(std::size_t);
  };
} // namespace application::cmd_menu

