#include "CommandMenu.h"

#include <CommandMenuConfig.h>
#include <Executor.h>
#include <algorithm>

namespace application::cmd_menu {

  CommandMenu::CommandMenu(const CommandMenuConfig& cfg)
      : m_prev_input_size(0ul)
      , m_search_engine(cfg) {}

  CommandMenu::~CommandMenu() = default;

  void CommandMenu::process_input_update(const QString& input) noexcept {
    auto suggestions = (input.size() > m_prev_input_size)
                           ? m_search_engine.push(input.toStdString().back())
                           : m_search_engine.pop();

    emit suggestion_page_prepared(suggestions ? suggestions->get()
                                              : Suggestions());

    emit suggestions_count_updated(suggestions ? suggestions->get().size()
                                               : 0ul);

    m_prev_input_size = input.size();
  }

  void CommandMenu::suggestion_page_requested(unsigned int start,
                                              unsigned int count) noexcept {
    Suggestions suggestions;
    if (count == 0u) {
      emit suggestion_page_prepared(suggestions);
      return;
    }

    const auto& last_suggestions = m_search_engine.suggestions();

    if (!last_suggestions) {
      emit suggestions_page_rejected();
      return;
    }

    if (start >= last_suggestions->get().size()) {
      emit suggestions_page_rejected();
      return;
    }

    auto page_start = std::next(last_suggestions->get().begin(), start);
    auto page_end = std::next(last_suggestions->get().begin(), start + count);

    if (auto page_size = start + count;
        std::distance(page_start, last_suggestions->get().end()) < page_size) {
      page_end = last_suggestions->get().end();
    }

    std::copy(page_start, page_end,
              std::inserter(suggestions, suggestions.end()));
    emit suggestion_page_prepared(suggestions);
  }

  void
  CommandMenu::apply_suggestion(const QString& selected_suggestion) noexcept {
    Executor e;
    e.execute(selected_suggestion.toStdString());
  }

  void CommandMenu::process_input_erase() noexcept {
    m_search_engine.clear();
    m_prev_input_size = 0ul;
  }
} // namespace application::cmd_menu
