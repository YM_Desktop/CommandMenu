#pragma once
#include <string>

namespace application::cmd_menu {

  /**
   * @class Executor
   * @brief Executes command selected by user.
   */
  class Executor {
  public:
    /**
     * @fn Executor
     * @brief Constructor.
     */
    Executor();

    /**
     * @fn ~Executor
     * @brief Destructor.
     */
    ~Executor();

    /**
     * @fn application::cmd_menu::Executor::execute
     * @brief Executes command provided by param.
     * @param cmd : string
     */
    void execute(std::string&& cmd) const noexcept;
  };
} // namespace application::cmd_menu
