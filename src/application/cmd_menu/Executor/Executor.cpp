#include "Executor.h"

#include <cstdlib>
#include <thread>

namespace application::cmd_menu {

  Executor::Executor() = default;
  Executor::~Executor() = default;

  void Executor::execute(std::string&& cmd) const noexcept {
    std::thread([cmd = std::move(cmd)]() {
      std::system(cmd.c_str());
    }).detach();
  }
} // namespace application::cmd_menu

