#pragma once

#include <set>
#include <string_view>

namespace application::cmd_menu {

  /**
   * @struct Suggestion
   * @brief Search suggestion.
   */
  struct Suggestion {
    /**
     * @brief Binary name.
     */
    std::string_view binary;

    /**
     * @brief Binary prefix(dir) name.
     */
    std::string_view prefix;

    /**
     * @fn Suggestion
     * @brief Constructor
     * @param binary_name
     * @param prefix_name
     */
    Suggestion(std::string_view binary_name, std::string_view prefix_name)
        : binary(binary_name)
        , prefix(prefix_name) {}

    /**
     * @brief Comparing '<' operator.
     * @param other: Suggestion to compare.
     * @return true, if Suggestion 'lesser', false otherwise.
     */
    constexpr inline bool operator<(const Suggestion& other) const noexcept {
      return binary < other.binary;
    }

    /**
     * @brief Comparing '==' operator.
     * @param other: Suggestion to compare.
     * @return true, if Suggestion 'equals', false otherwise.
     */
    constexpr inline bool operator==(const Suggestion& other) const noexcept {
      return binary == other.binary && prefix == other.prefix;
    }
  };

  /**
   * @brief Data type for search suggestions.
   */
  using Suggestions = std::set<Suggestion>;

} // namespace application::cmd_menu

