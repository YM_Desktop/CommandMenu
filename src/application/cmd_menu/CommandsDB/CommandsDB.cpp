#include "CommandsDB.h"

#include <CommandMenuConfig.h>
#include <cstdlib>
#include <filesystem>
#include <list>
#include <utility>

namespace {
  std::set<std::string> collect_binaries(const std::filesystem::path& path) {
    std::set<std::string> binaries;

    std::filesystem::directory_iterator dir_iterator(path);
    for (const auto& entry : dir_iterator) {

      bool is_file = entry.is_regular_file();
      if (!is_file)
        continue;

      const auto permissions = entry.status().permissions();
      bool is_executable =
          ((std::filesystem::perms::owner_exec & permissions) !=
           std::filesystem::perms::none) ||
          ((std::filesystem::perms::group_exec & permissions) !=
           std::filesystem::perms::none) ||
          ((std::filesystem::perms::others_exec & permissions) !=
           std::filesystem::perms::none);
      if (!is_executable)
        continue;

      binaries.emplace(entry.path().filename());
    }

    return binaries;
  }

  std::list<std::filesystem::path> parse_env_path_to_paths() noexcept {

    std::list<std::filesystem::path> paths;

    if (std::string env_path = std::getenv("PATH"); !env_path.empty()) {

      std::string_view env_path_view = env_path;
      std::size_t start_index{0ul};
      std::size_t sub_index{env_path_view.find_first_of(':')};

      do {
        paths.emplace_back(
            env_path_view.substr(start_index, sub_index - start_index));
        start_index = sub_index + 1;
        sub_index = env_path_view.find_first_of(':', start_index);
      } while (sub_index != std::string::npos);

      if (start_index != std::string::npos) {
        paths.emplace_back(env_path_view.substr(start_index));
      }
    }

    return paths;
  }
} // namespace

namespace application::cmd_menu {

  CommandsDB::CommandsDB(const CommandMenuConfig& cfg) {
    if (cfg.use_system_path) {
      auto system_paths = parse_env_path_to_paths();
      for (const auto& path : system_paths) {
        m_binaries.emplace(std::string(path.c_str()), collect_binaries(path));
      }
    }

    for (const auto& path : cfg.custom_search_paths) {
      m_binaries.emplace(std::string(path.c_str()), collect_binaries(path));
    }
  }

  CommandsDB::~CommandsDB() = default;

  const CommandsBinariesMap& CommandsDB::binaries() const noexcept {
    return m_binaries;
  }
} // namespace application::cmd_menu
