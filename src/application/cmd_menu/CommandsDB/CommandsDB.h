#pragma once
#include <set>
#include <string>
#include <unordered_map>

namespace application::cmd_menu {
  struct CommandMenuConfig;
}

namespace application::cmd_menu {
  using CommandsBinariesMap =
      std::unordered_map<std::string, std::set<std::string>>;
}

namespace application::cmd_menu {

  /**
   * @class CommandsDB
   * @brief Represents commands database.
   */
  class CommandsDB {

    /**
     * @brief map of paths to binaries names.
     */
    CommandsBinariesMap m_binaries;

  public:
    /**
     * @fn CommandsDB
     * @brief Constructor
     * @param cfg: CommandMenuConfig
     */
    CommandsDB(const CommandMenuConfig& cfg);

    /**
     * @fn ~CommandsDB
     * @brief Destructor.
     */
    ~CommandsDB();

    /**
     * @fn binaries
     * @brief gives readonly access to binaries map.
     * @return const CommandsBinariesMap&.
     */
    const CommandsBinariesMap& binaries() const noexcept;
  };
} // namespace application::cmd_menu
