#include "CommandMenuApplication.h"

#include <ApplicationConfig.h>
#include <CommandMenu.h>
#include <CommandMenuWindow.h>
#include <ConfigParser.h>
#include <QCommandLineParser>
#include <QObject>

namespace {
  std::unique_ptr<application::ApplicationConfig>
  read_and_parse_config(const QApplication& app) {
    QCommandLineParser cmd_parser;
    QCommandLineOption config_option("config", "Specifies config path",
                                     "config.json");
    cmd_parser.addOption(config_option);
    cmd_parser.process(app);

    std::filesystem::path config_path =
        cmd_parser.value(config_option).toStdString();

    return application::config::parsing::ConfigParser::parse(config_path);
  }
} // namespace

namespace application {

  CommandMenuApplication::CommandMenuApplication(int argc, char** argv)
      : QApplication(argc, argv) {

    auto cfg = read_and_parse_config(*this);
    m_menu = std::make_unique<cmd_menu::CommandMenu>(cfg->menu_cfg);
    m_window = std::make_unique<ui::CommandMenuWindow>(cfg->ui_cfg);

    QObject::connect(m_window.get(), &ui::CommandMenuWindow::input_updated,
                     m_menu.get(),
                     &application::cmd_menu::CommandMenu::process_input_update);
    QObject::connect(m_window.get(), &ui::CommandMenuWindow::input_erased,
                     m_menu.get(),
                     &application::cmd_menu::CommandMenu::process_input_erase);
    QObject::connect(m_window.get(),
                     &ui::CommandMenuWindow::suggestion_selected, m_menu.get(),
                     &application::cmd_menu::CommandMenu::apply_suggestion);
    QObject::connect(
        m_window.get(), &ui::CommandMenuWindow::suggestion_page_requested,
        m_menu.get(),
        &application::cmd_menu::CommandMenu::suggestion_page_requested);
    QObject::connect(
        m_menu.get(),
        &application::cmd_menu::CommandMenu::suggestion_page_prepared,
        m_window.get(), &ui::CommandMenuWindow::suggestion_page_prepared);
    QObject::connect(
        m_menu.get(),
        &application::cmd_menu::CommandMenu::suggestions_page_rejected,
        m_window.get(), &ui::CommandMenuWindow::suggestions_page_rejected);

    QObject::connect(
        m_menu.get(),
        &application::cmd_menu::CommandMenu::suggestions_count_updated,
        m_window.get(), &ui::CommandMenuWindow::suggestions_count_updated);

    m_window->show();
  }

  CommandMenuApplication::~CommandMenuApplication() = default;
} // namespace application
