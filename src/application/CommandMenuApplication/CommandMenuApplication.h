#pragma once
#include <QApplication>
#include <memory>

namespace application {
  struct ApplicationConfig;
}

namespace application::cmd_menu {
  class CommandMenu;
}

namespace application::ui {
  class CommandMenuWindow;
}

namespace application {
  /**
   * @class CommandMenuApplication
   * @brief Represents CommandMenu application logic.
   */
  class CommandMenuApplication : public QApplication {

    /**
     * @brief CommandMenu instance (backend).
     */
    std::unique_ptr<application::cmd_menu::CommandMenu> m_menu;

    /**
     * @brief CommandMenuWindow instance (frontend/ui).
     */
    std::unique_ptr<ui::CommandMenuWindow> m_window;

  public:
    /**
     * @fn CommandMenuApplication
     * @brief Constructor
     * @param argc: number of arguments provided
     * @param argv: arguments array
     */
    CommandMenuApplication(int argc, char** argv);

    /**
     * @fn ~CommandMenuApplication
     * @brief Destructor
     */
    ~CommandMenuApplication();
  };
} // namespace application
