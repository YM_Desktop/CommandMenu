#include <ApplicationConfig.h>
#include <CommandMenuApplication.h>
#include <iostream>
#include <memory>
#include <yaml-cpp/exceptions.h>

int run_app(int argc, char** argv) {

  try {

    application::CommandMenuApplication app(argc, argv);
    return app.exec();

  } catch (const std::logic_error& e) {
    std::cerr << e.what() << "\n";
  } catch (const YAML::BadConversion& e) {
    std::cerr << e.what() << "\n";
  } catch (...) {
    std::cerr << "Unexpected error!\n";
  }

  return 1;
}

int main(int argc, char** argv) { return run_app(argc, argv); }
